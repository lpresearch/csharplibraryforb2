﻿using LPMSB2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
public class DataLogger
{
    /////////////////////////
    //Flash Debug Data Log
    /////////////////////////

    Boolean isDebugLoggingStarted = false;
    System.IO.StreamWriter DebuglogFile;
    private readonly object DebuglogFileLock = new object();

    public Boolean isDebugLogging()
    {
        return isDebugLoggingStarted;

    }

    public Boolean startDebugLogging(String filename)
    {
        Boolean status = false;
        DebuglogFile =
             new System.IO.StreamWriter(filename);

        DebuglogFile.WriteLine(
                    "SensorId," +
                    "TimeStamp (s)," +
                    "FrameNumber," +
                    " AccX (g), AccY (g), AccZ (g), " +
                    "GyroX (deg/s), GyroY (deg/s), GyroZ (deg/s), " +
                    "MagX (uT), MagY (uT), MagZ (uT), " +
                    "QuatW, QuatX, QuatY, QuatZ, " +
                    "Pressure(kPa)," +
                    "Temperature(degC)");

        isDebugLoggingStarted = true;
        status = true;

        return status;
    }

    public Boolean stopDebugLogging()
    {
        Boolean status = false;
        if (!isDebugLoggingStarted)
        {
            return status;
        }

        isDebugLoggingStarted = false;
        lock (DebuglogFileLock)
        {
            DebuglogFile.Close();
        }

        status = true;

        return status;
    }

    public Boolean logDebugLpmsData(LpmsData d)
    {
        Boolean status = false;
        if (isDebugLoggingStarted)
        {
            lock (DebuglogFileLock)
            {
                DebuglogFile.WriteLine(
                            d.imuId + ","
                            + d.timestamp + ", "
                            + d.frameNumber + ","
                            + d.acc[0] + ", " + d.acc[1] + ", " + d.acc[2] + ", "
                            + d.gyr[0] + ", " + d.gyr[1] + ", " + d.gyr[2] + ", "
                            + d.mag[0] + ", " + d.mag[1] + ", " + d.mag[2] + ", "
                            + d.quat[0] + ", " + d.quat[1] + ", " + d.quat[2] + ", " + d.quat[3] + ", "
                            + d.pressure + ", "
                            + d.temperature);
            }
            status = true;

        }
        return status;
    }

}