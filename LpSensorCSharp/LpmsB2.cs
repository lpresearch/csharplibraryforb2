﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Collections;
using InTheHand.Net;
using InTheHand.Net.Bluetooth;
using InTheHand.Net.Sockets;

namespace LPMSB2
{
    public class LpmsB2
    {
        //////////////////////////////////////////////
        // Public definitions
        //////////////////////////////////////////////
        public const int LPMS_STREAM_FREQ_5HZ           = 5;
        public const int LPMS_STREAM_FREQ_10HZ          = 10;
        public const int LPMS_STREAM_FREQ_25HZ          = 25;
        public const int LPMS_STREAM_FREQ_50HZ          = 50;
        public const int LPMS_STREAM_FREQ_100HZ         = 100;
        public const int LPMS_STREAM_FREQ_200HZ         = 200;
        public const int LPMS_STREAM_FREQ_400HZ         = 400;

        public const int LPMS_FILTER_GYR                = 0;
        public const int LPMS_FILTER_GYR_ACC            = 1;
        public const int LPMS_FILTER_GYR_ACC_MAG        = 2;
        public const int LPMS_FILTER_DCM_GYR_ACC        = 3;
        public const int LPMS_FILTER_DCM_GYR_ACC_MAG    = 4;
        //Magnetometer correction
        public const int LPMS_FILTER_PRESET_DYNAMIC     = 0;
        public const int LPMS_FILTER_PRESET_STRONG      = 1;
        public const int LPMS_FILTER_PRESET_MEDIUM      = 2;
        public const int LPMS_FILTER_PRESET_WEAK        = 3;
        //Low Pass Filter
        public const int LPMS_LOW_FILTER_OFF            = 0;
        public const int LPMS_LOW_FILTER_40HZ           = 1;
        public const int LPMS_LOW_FILTER_20HZ           = 2;
        public const int LPMS_LOW_FILTER_4HZ            = 3;
        public const int LPMS_LOW_FILTER_2HZ            = 4;
        public const int LPMS_LOW_FILTER_04HZ           = 5;
        // Gyro Range
        public const int LPMS_GYR_RANGE_125DPS          = 125;
        public const int LPMS_GYR_RANGE_245DPS          = 245;
        public const int LPMS_GYR_RANGE_250DPS          = 250;
        public const int LPMS_GYR_RANGE_500DPS          = 500;
        public const int LPMS_GYR_RANGE_1000DPS         = 1000;
        public const int LPMS_GYR_RANGE_2000DPS         = 2000;
        // Acc Range
        public const int LPMS_ACC_RANGE_2G              = 2;
        public const int LPMS_ACC_RANGE_4G              = 4;
        public const int LPMS_ACC_RANGE_8G              = 8;
        public const int LPMS_ACC_RANGE_16G             = 16;
        // Mag Range
        public const int LPMS_MAG_RANGE_4GAUSS          = 4;
        public const int LPMS_MAG_RANGE_8GAUSS          = 8;
        public const int LPMS_MAG_RANGE_12GAUSS         = 12;
        public const int LPMS_MAG_RANGE_16GAUSS         = 16;
        // Connection status
        public const int SENSOR_STATUS_CONNECTED        = 1;
        public const int SENSOR_STATUS_CONNECTING       = 2;
        public const int SENSOR_STATUS_DISCONNECTED     = 3;
        public const int SENSOR_STATUS_ERROR            = 4;
        // Offset mode
        public const int LPMS_OFFSET_MODE_OBJECT        = 0;
        public const int LPMS_OFFSET_MODE_HEADING       = 1;
        public const int LPMS_OFFSET_MODE_ALIGNMENT     = 2;

        //Flash State
        public const int FLASH_STATE_OK                 = 0;
        public const int FLASH_STATE_MEMORY_FULL        = 1;
        public const int FLASH_STATE_META_TABLE_FULL    = 2;
        public const int FLASH_STATE_MEMORY_ERROR       = 3;

        // Configuration register contents
        public const int LPMS_GYR_AUTOCAL_ENABLED               = 0x00000001 << 30;
        public const int LPMS_LPBUS_DATA_MODE_16BIT_ENABLED     = 0x00000001 << 22;
        public const int LPMS_LINACC_OUTPUT_ENABLED             = 0x00000001 << 21;
        public const int LPMS_DYNAMIC_COVAR_ENABLED             = 0x00000001 << 20;
        public const int LPMS_ALTITUDE_OUTPUT_ENABLED           = 0x00000001 << 19;
        public const int LPMS_QUAT_OUTPUT_ENABLED               = 0x00000001 << 18;
        public const int LPMS_EULER_OUTPUT_ENABLED              = 0x00000001 << 17;
        public const int LPMS_ANGULAR_VELOCITY_OUTPUT_ENABLED   = 0x00000001 << 16;
        public const int LPMS_GYR_CALIBRA_ENABLED               = 0x00000001 << 15;
        public const int LPMS_HEAVEMOTION_OUTPUT_ENABLED        = 0x00000001 << 14;
        public const int LPMS_TEMPERATURE_OUTPUT_ENABLED        = 0x00000001 << 13;
        public const int LPMS_GYR_RAW_OUTPUT_ENABLED            = 0x00000001 << 12;
        public const int LPMS_ACC_RAW_OUTPUT_ENABLED            = 0x00000001 << 11;
        public const int LPMS_MAG_RAW_OUTPUT_ENABLED            = 0x00000001 << 10;
        public const int LPMS_PRESSURE_OUTPUT_ENABLED           = 0x00000001 << 9;


        //////////////////////////////////////////////
        // Private definitions
        //////////////////////////////////////////////
        const String TAG                                = "LpmsB2";
        const int PACKET_ADDRESS0                       = 0;
        const int PACKET_ADDRESS1                       = 1;
        const int PACKET_FUNCTION0                      = 2;
        const int PACKET_FUNCTION1                      = 3;
        const int PACKET_LENGTH0                        = 4;
        const int PACKET_LENGTH1                        = 5;
        const int PACKET_RAW_DATA                       = 6;
        const int PACKET_LRC_CHECK0                     = 7;
        const int PACKET_LRC_CHECK1                     = 8;
        const int PACKET_END                            = 9;

        //////////////////////////////////////////////
        // Command Registers
        //////////////////////////////////////////////
        const int REPLY_ACK                     = 0;
        const int REPLY_NACK                    = 1;
        const int UPDATE_FIRMWARE               = 2;
        const int UPDATE_IAP                    = 3;
        const int GET_CONFIG                    = 4;
        const int GET_STATUS                    = 5;
        const int GOTO_COMMAND_MODE             = 6;
        const int GOTO_STREAM_MODE              = 7;
        const int GET_SENSOR_DATA               = 9;
        const int SET_TRANSMIT_DATA             = 10;
        const int SET_STREAM_FREQ               = 11;
        // Register value save and reset
        const int WRITE_REGISTERS               = 15;
        const int RESTORE_FACTORY_VALUE         = 16;
        // Reference setting and offset reset
        const int RESET_REFERENCE               = 17;
        const int SET_ORIENTATION_OFFSET        = 18;
        const int RESET_ORIENTATION_OFFSET      = 82;
        //IMU ID setting
        const int SET_IMU_ID                    = 20;
        const int GET_IMU_ID                    = 21;
        // Gyroscope settings
        const int START_GYR_CALIBRA             = 22;
        const int ENABLE_GYR_AUTOCAL            = 23;
        const int ENABLE_GYR_THRES              = 24;
        const int SET_GYR_RANGE                 = 25;
        const int GET_GYR_RANGE                 = 26;
        // Accelerometer settings
        const int SET_ACC_BIAS                  = 27;
        const int GET_ACC_BIAS                  = 28;
        const int SET_ACC_ALIGN_MATRIX          = 29;
        const int GET_ACC_ALIGN_MATRIX          = 30;
        const int SET_ACC_RANGE                 = 31;
        const int GET_ACC_RANGE                 = 32;
        const int SET_GYR_ALIGN_BIAS            = 48;
        const int GET_GYR_ALIGN_BIAS            = 49;
        const int SET_GYR_ALIGN_MATRIX          = 50;
        const int GET_GYR_ALIGN_MATRIX          = 51;
        // Magnetometer settings
        const int SET_MAG_RANGE                 = 33;
        const int GET_MAG_RANGE                 = 34;
        const int SET_HARD_IRON_OFFSET          = 35;
        const int GET_HARD_IRON_OFFSET          = 36;
        const int SET_SOFT_IRON_MATRIX          = 37;
        const int GET_SOFT_IRON_MATRIX          = 38;
        const int SET_FIELD_ESTIMATE            = 39;
        const int GET_FIELD_ESTIMATE            = 40;
        const int SET_MAG_ALIGNMENT_MATRIX      = 76;
        const int SET_MAG_ALIGNMENT_BIAS        = 77;
        const int SET_MAG_REFRENCE              = 78;
        const int GET_MAG_ALIGNMENT_MATRIX      = 79;
        const int GET_MAG_ALIGNMENT_BIAS        = 80;
        const int GET_MAG_REFERENCE             = 81;
        // Filter settings
        const int SET_FILTER_MODE               = 41;
        const int GET_FILTER_MODE               = 42;
        const int SET_FILTER_PRESET             = 43;
        const int GET_FILTER_PRESET             = 44;
        const int SET_LIN_ACC_COMP_MODE         = 67;
        const int GET_LIN_ACC_COMP_MODE         = 68;

        //////////////////////////////////////////////
        // Status register contents
        //////////////////////////////////////////////
        const int SET_CENTRI_COMP_MODE          = 69;
        const int GET_CENTRI_COMP_MODE          = 70;
        const int SET_RAW_DATA_LP               = 60;
        const int GET_RAW_DATA_LP               = 61;
        const int SET_TIME_STAMP                = 66;
        const int SET_LPBUS_DATA_MODE           = 75;
        const int GET_FIRMWARE_VERSION          = 47;
        const int GET_BATTERY_LEVEL             = 87;
        const int GET_BATTERY_VOLTAGE           = 88;
        const int GET_CHARGING_STATUS           = 89;
        const int GET_SERIAL_NUMBER             = 90;
        const int GET_DEVICE_NAME               = 91;
        const int GET_FIRMWARE_INFO             = 92;
        const int START_SYNC                    = 96;
        const int STOP_SYNC                     = 97;
        const int GET_PING                      = 98;
        const int GET_TEMPERATURE               = 99;

        //////////////////////////////////////////////
        // Debug Flash Logging
        //////////////////////////////////////////////
        const int START_DEBUG_LOGGING           = 110;
        const int STOP_DEBUG_LOGGING            = 111;
        const int CLEAR_DEBUG_LOG               = 112;
        const int FULL_FLASH_ERASE              = 113;
        const int GET_DEBUG_LOGGING_STATUS      = 114;
        const int GET_DEBUG_LOG_TABLE_SIZE      = 115;
        const int GET_DEBUG_LOG_TABLE           = 116;
        const int GET_DEBUG_LOG_SIZE            = 117;
        const int GET_DEBUG_LOG                 = 118;
        const int GET_FLASH_MEMORY_STATE        = 119;


        const int LPMS_STREAM_FREQ_5HZ_ENABLED                  = 0x00000000;
        const int LPMS_STREAM_FREQ_10HZ_ENABLED                 = 0x00000001;
        const int LPMS_STREAM_FREQ_25HZ_ENABLED                 = 0x00000002;
        const int LPMS_STREAM_FREQ_50HZ_ENABLED                 = 0x00000003;
        const int LPMS_STREAM_FREQ_100HZ_ENABLED                = 0x00000004;
        const int LPMS_STREAM_FREQ_200HZ_ENABLED                = 0x00000005;
        const int LPMS_STREAM_FREQ_400HZ_ENABLED                = 0x00000006;
        const int LPMS_STREAM_FREQ_MASK                         = 0x00000007;

        const int MAX_BUFFER = 2048;
        const int DATA_QUEUE_SIZE = 64;
        const float r2d = 57.2958f;
        const int PARAMETER_SET_DELAY = 1;
        const int ACK_WAIT = 0;
        const int ACK_RECEIVED = 1;
        const int NACK_RECEIVED = 2;

        // status
        int connectionStatus = SENSOR_STATUS_DISCONNECTED;
        String sensorErrorMessage = "none";

        // Protocol parsing related
        int rxState = PACKET_END;
        byte[] rxBuffer = new byte[MAX_BUFFER];
        byte[] txBuffer = new byte[MAX_BUFFER];
        byte[] rawTxData = new byte[MAX_BUFFER];
        byte[] rawRxBuffer = new byte[MAX_BUFFER];
        int currentAddress = 0;
        int currentFunction = 0;
        int currentLength = 0;
        int rxIndex = 0;
        byte b = 0;
        int lrcCheck = 0;
        int nBytes = 0;
        int actStatus = ACK_WAIT;
        bool waitForData = false;
        byte[] inBytes = new byte[2];

        // Serial Communication
        String mAddress;
        Guid serviceUuid = new Guid("00001101-0000-1000-8000-00805F9B34FB");
        BluetoothRadio radio;
        BluetoothClient btCli;
        Stream btStream;

        // Settings related
        int imuId = 0;
        int gyrRange = 0;
        int accRange = 0;
        int magRange = 0;
        int streamingFrequency = 0;
        int filterMode = 0;
        int magCorrection = 0;
        int lowPassFilter = 0;
        bool isStreamMode = false;

        int configurationRegister = 0;
        bool configurationRegisterReady = false;
        String serialNumber = "";
        bool serialNumberReady = false;
        String deviceName = "";
        bool deviceNameReady = false;
        String firmwareInfo = "";
        bool firmwareInfoReady = false;
        String firmwareVersion ="";

        bool accEnable = false;
        bool gyrEnable = false;
        bool magEnable = false;
        bool angularVelEnable = false;
        bool quaternionEnable = false;
        bool eulerAngleEnable = false;
        bool linAccEnable = false;
        bool pressureEnable = false;
        bool altitudeEnable = false;
        bool temperatureEnable = false;
        bool heaveEnable = false;
        bool sixteenBitDataEnable = false;
        bool resetTimestampFlag = false;

        // Sensor Data
        bool newDataFlag = false;
        Queue<LpmsData> dataQueue = new Queue<LpmsData>(DATA_QUEUE_SIZE);
        private Object lockObject = new Object();
        LpmsData mLpmsData = new LpmsData();
        int frameCounter = 0;

        // Data logger
        DataLogger dl = new DataLogger();

        // Flash
        const int LogEntrySize = 64;
        int debugLoggingStatus = 1;
        int debugLogSize = 0;
        int debugLogSizeIndex = 0;
        int updateProgress = 0;
        int flashState = FLASH_STATE_OK;

        Thread connectionThread;
        Thread dataReadThread;

        // Debug
        bool verbose = false;
        /*
            Function: LpmsB2 constructor
            Constructor for LpmsB2 object
            Parameters:
                LpmsB2 bluetooth address in format: 00:11:22:33:44:55
        */
        public LpmsB2(String address) {
            mAddress = address;
            connectionStatus = SENSOR_STATUS_DISCONNECTED;
        }

        /*
            Function: connect()
            connects to sensor
            Parameters:
                none
            Returns:
                int connectionStatus

                connectionStatus list:
                SENSOR_STATUS_CONNECTED = 1;
                SENSOR_STATUS_CONNECTING = 2;
                SENSOR_STATUS_DISCONNECTED = 3;
                SENSOR_STATUS_ERROR = 4;
        */
        public int connect()
        {
            if (!_checkBluetooth())
            {
                _setErrorMessage("No bluetooth adapter found");
                connectionStatus = SENSOR_STATUS_ERROR;
                return connectionStatus;
            }

            // Check for active connection
            if (connectionStatus == SENSOR_STATUS_CONNECTED)
            {
                _setErrorMessage("Another connection established");
                return connectionStatus;
            }
            else if (connectionStatus == SENSOR_STATUS_CONNECTING)
            {
                _setErrorMessage("Another connection establishing");
                return connectionStatus;
            }

            // Start connection/reading thread
            connectionThread = new Thread(_startSensorConnection);
            connectionThread.Start();
            connectionStatus = SENSOR_STATUS_CONNECTING;
            return connectionStatus;
        }

        /*
            Function: disconnect()
            disconnects to sensor
            Parameters:
                none
            Returns:
                bool success
                return true on sensor disconnection success
                return false if sensor disconnection failed
        */
        public bool disconnect()
        {
            bool res = false;
            if (connectionStatus == SENSOR_STATUS_DISCONNECTED)
                return true;

            if (btCli == null)
            {
                connectionStatus = SENSOR_STATUS_DISCONNECTED;
                return true;
            }
            try
            {

                if (btStream != null)
                {
                    btStream.ReadTimeout = 500;
                    btStream.WriteTimeout = 500;
                    btStream.Close();
                }
                if (btCli.Connected)
                    btCli.Close();
                btCli.Dispose();
                res = !btCli.Connected;
            }
            catch(IOException e)
            {
                Console.WriteLine(e);
            }
            connectionStatus = SENSOR_STATUS_DISCONNECTED;
            return res;
        }

        /*
            Function: isConnect()
            check if sensor connection is established
            Parameters:
                none
            Returns:
                bool connectionStatus
                return true if sensor is connected
                return false if sensor is not connected
        */
        public bool isConnected()
        {
            return connectionStatus == SENSOR_STATUS_CONNECTED;
        }

        /*
            Function: setAddress(String address)
            set bluetooth address to connects to
            Parameters:
                String address: sensor bluetooth address
            Returns:
                none
        */
        public void setAddress(String address)
        {
            mAddress = address;
        }

        /*
            Function: getAddress()
            get current sensor bluetooth address
            Parameters:
                none
            Returns:
                String address: sensor bluetooth address
        */
        public String getAddress()
        {
            return mAddress;
        }

        /*
            Function: setCommandMode()
            put sensor into command mode
            Parameters:
                none
            Returns:
                bool:  true: success  false: fail
        */
        public bool setCommandMode()
        {
            if (!_assertConnected())
                return false;
            actStatus = ACK_WAIT;
            lpbusSetNone(GOTO_COMMAND_MODE);
            if (verbose) Console.WriteLine(TAG+"  Send GOTO_COMMAND_MODE");

            if (_waitForAckLoop() == ACK_RECEIVED)
            {
                isStreamMode = false;
                return true;
            }

            return false;
        }

        /*
            Function: setStreamingMode()
            put sensor into streaming mode
            Parameters:
                none
            Returns:
                bool:  true: success  false: fail
        */
        public bool setStreamingMode()
        {
            if (!_assertConnected())
                return false;
            actStatus = ACK_WAIT;
            if (verbose) Console.WriteLine(TAG+"  Send GOTO_STREAM_MODE");
            lpbusSetNone(GOTO_STREAM_MODE);
            if (_waitForAckLoop() == ACK_RECEIVED)
            {
                isStreamMode = true;
                return true;
            }
            return false;
        }
 
        /*
            Function: saveParameters()
            save settings to sensor
            Parameters:
                none
            Returns:
                bool:  true: success  false: fail
        */
        public bool saveParameters()
        {
            if (!_assertConnected())
                return false;
            return _saveParameters();
        }
        /*
            Function: setImuId(int id)
            set sensor id
            Parameters:
                int id
            Returns:
                bool:  true: success  false: fail
        */
        public bool setImuId(int id)
        {
            if (!_assertConnected())
                return false;

            actStatus = ACK_WAIT;
            lpbusSetInt32(SET_IMU_ID, id);
            if (_waitForAckLoop() == ACK_RECEIVED)
            {
                imuId = id;
                return true;
            }

            return false;
        }

        /*
            Function: getImuId()
            get sensor id
            Parameters:
                none
            Returns:
                int id
        */
        public int getImuId()
        {
            return imuId;
        }

        /*
            Function: getGyroRange()
            get sensor gyro range
            Parameters:
            none
            Returns:
            int gyro range

            Gyro range list:
                LPMS_GYR_RANGE_125DPS = 125;
                LPMS_GYR_RANGE_245DPS = 245;
                LPMS_GYR_RANGE_250DPS = 250;
                LPMS_GYR_RANGE_500DPS = 500;
                LPMS_GYR_RANGE_1000DPS = 1000;
                LPMS_GYR_RANGE_2000DPS = 2000;
        */
        public int getGyroRange()
        {
            return gyrRange;
        }

        /*
            Function: setGyroRange(int range)
            set sensor gyro range
            Parameters: 
            int gyro range

            Gyro range list:
                LPMS_GYR_RANGE_125DPS = 125;
                LPMS_GYR_RANGE_245DPS = 245;
                LPMS_GYR_RANGE_250DPS = 250;
                LPMS_GYR_RANGE_500DPS = 500;
                LPMS_GYR_RANGE_1000DPS = 1000;
                LPMS_GYR_RANGE_2000DPS = 2000;
            Returns:
                bool:  true: success  false: fail
        */
        public bool setGyroRange(int range)
        {
            if (!_assertConnected())
                return false;
            if (range == LPMS_GYR_RANGE_125DPS ||
                    range == LPMS_GYR_RANGE_245DPS ||
                    range == LPMS_GYR_RANGE_500DPS ||
                    range == LPMS_GYR_RANGE_1000DPS ||
                    range == LPMS_GYR_RANGE_2000DPS)
            {
                actStatus = ACK_WAIT;
                lpbusSetInt32(SET_GYR_RANGE, range);
                if (_waitForAckLoop() == ACK_RECEIVED) {
                    gyrRange = range;
                    return true;
                }

            }
            return false;
        }

        /*
            Function: getAccRange()
            get sensor acc range
            Parameters:
            none
            Returns:
            int acc range

            Acc range list:
                LPMS_ACC_RANGE_2G = 2;
                LPMS_ACC_RANGE_4G = 4;
                LPMS_ACC_RANGE_8G = 8;
                LPMS_ACC_RANGE_16G = 16;
        */
        public int getAccRange()
        {
            return accRange;
        }

        /*
            Function: setAccRange(int range)
            set sensor acc range
            Parameters: 
            int acc range

            Acc range list:
                LPMS_ACC_RANGE_2G = 2;
                LPMS_ACC_RANGE_4G = 4;
                LPMS_ACC_RANGE_8G = 8;
                LPMS_ACC_RANGE_16G = 16;
            Returns:
                bool:  true: success  false: fail
        */
        public bool setAccRange(int range)
        {
            if (!_assertConnected())
                return false;
            if (range == LPMS_ACC_RANGE_2G ||
                    range == LPMS_ACC_RANGE_4G ||
                    range == LPMS_ACC_RANGE_8G ||
                    range == LPMS_ACC_RANGE_16G)
            {
                actStatus = ACK_WAIT;
                lpbusSetInt32(SET_ACC_RANGE, range);
                if (_waitForAckLoop() == ACK_RECEIVED) {
                    accRange = range;
                    return true;
                }
            }
            return false;
        }

        /*
            Function: getMagRange()
            get sensor mag range
            Parameters:
               none
            Returns:
               int mag range

               Mag range list:
                    LPMS_MAG_RANGE_4GAUSS = 4;
                    LPMS_MAG_RANGE_8GAUSS = 8;
                    LPMS_MAG_RANGE_12GAUSS = 12;
                    LPMS_MAG_RANGE_16GAUSS = 16;
       */
        public int getMagRange()
        {
            return magRange;
        }

        /*
            Function: setMagRange(int range)
            set sensor mag range
            Parameters: 
                int mag range

                Mag range list:
                        LPMS_MAG_RANGE_4GAUSS = 4;
                        LPMS_MAG_RANGE_8GAUSS = 8;
                        LPMS_MAG_RANGE_12GAUSS = 12;
                        LPMS_MAG_RANGE_16GAUSS = 16;
            Returns:
                bool:  true: success  false: fail
        */
        public bool setMagRange(int range)
        {
            if (!_assertConnected())
                return false;
            if (range == LPMS_MAG_RANGE_4GAUSS ||
                    range == LPMS_MAG_RANGE_8GAUSS ||
                    range == LPMS_MAG_RANGE_12GAUSS ||
                    range == LPMS_MAG_RANGE_16GAUSS)
            {
                actStatus = ACK_WAIT;
                lpbusSetInt32(SET_MAG_RANGE, range);
                if (_waitForAckLoop() == ACK_RECEIVED) {
                    magRange = range;
                    return true;
                }
            }
            return false;
        }

        /*
            Function: getFilterMode()
                get sensor filter mode
            Parameters:
                none
            Returns:
                int filter mode

                Filter mode list:
                Gyro only:                  LPMS_FILTER_GYR = 0;
                Gyro + Acc (Kalman):        LPMS_FILTER_GYR_ACC = 1;
                Gyro + Acc + Mag (Kalman):  LPMS_FILTER_GYR_ACC_MAG = 2;
                Gyro + Acc (DCM):           LPMS_FILTER_DCM_GYR_ACC = 3;
                Gyro + Acc + Mag (DCM):     LPMS_FILTER_DCM_GYR_ACC_MAG = 4;
        */
        public int getFilterMode()
        {
            return filterMode;
        }

        /*
            Function: setFilterMode(int mode)
            set sensor filter mode
            Parameters: 
                int filter mode

                Filter mode list:
                Gyro only:                  LPMS_FILTER_GYR = 0;
                Gyro + Acc (Kalman):        LPMS_FILTER_GYR_ACC = 1;
                Gyro + Acc + Mag (Kalman):  LPMS_FILTER_GYR_ACC_MAG = 2;
                Gyro + Acc (DCM):           LPMS_FILTER_DCM_GYR_ACC = 3;
                Gyro + Acc + Mag (DCM):     LPMS_FILTER_DCM_GYR_ACC_MAG = 4;
            Returns:
                bool:  true: success  false: fail
        */
        public bool setFilterMode(int mode)
        {
            if (!_assertConnected())
                return false;
            if (mode == LPMS_FILTER_GYR ||
                    mode == LPMS_FILTER_GYR_ACC ||
                    mode == LPMS_FILTER_GYR_ACC_MAG ||
                    mode == LPMS_FILTER_DCM_GYR_ACC ||
                    mode == LPMS_FILTER_DCM_GYR_ACC_MAG)
            {
                actStatus = ACK_WAIT;
                lpbusSetInt32(SET_FILTER_MODE, mode);
                if (_waitForAckLoop() == ACK_RECEIVED) 
                {
                    filterMode = mode;
                    return true;
                }
            }

            return false;
        }

        /*
            Function: getMAGCorrection()
                get sensor magnetic correction preset
            Parameters:
                none
            Returns:
                int magnetic correction preset

                Magnetic correction preset list:
                Dynamic: LPMS_FILTER_PRESET_DYNAMIC = 0;
                Strong:  LPMS_FILTER_PRESET_STRONG = 1;
                Medium:  LPMS_FILTER_PRESET_MEDIUM = 2;
                Weak:    LPMS_FILTER_PRESET_WEAK = 3;
        */
        public int getMAGCorrection()
        {
            return magCorrection;
        }

        /*
            Function: setMAGCorrection( int preset)
            set sensor magnetic correction preset
            Parameters: 
                int magnetic correction preset

                Magnetic correction preset list:
                Dynamic: LPMS_FILTER_PRESET_DYNAMIC = 0;
                Strong:  LPMS_FILTER_PRESET_STRONG = 1;
                Medium:  LPMS_FILTER_PRESET_MEDIUM = 2;
                Weak:    LPMS_FILTER_PRESET_WEAK = 3;
            Returns:
                bool:  true: success  false: fail
        */
        public bool setMAGCorrection( int preset)
        {
            if (!_assertConnected())
                return false;
            if (preset == LPMS_FILTER_PRESET_DYNAMIC ||
                    preset == LPMS_FILTER_PRESET_STRONG ||
                    preset == LPMS_FILTER_PRESET_MEDIUM ||
                    preset == LPMS_FILTER_PRESET_WEAK)
            {

                //ThreadPool.QueueUserWorkItem(new WaitCallback(SendingCommandThread), new CommandParameter(SET_FILTER_PRESET, preset));

                actStatus = ACK_WAIT;
                lpbusSetInt32(SET_FILTER_PRESET, preset);
                if (_waitForAckLoop() == ACK_RECEIVED) 
                {
                    magCorrection = preset;
                    return true;
                }
            }

            return false;
        }

        /*
            Function: getLowPassFilter()
            get sensor low pass filter settings, only affects raw data output
            Parameters:
                none
            Returns:
                int low pass filter settings

                low pass filter settings list:
                LPMS_LOW_FILTER_OFF = 0;
                LPMS_LOW_FILTER_40HZ = 1;
                LPMS_LOW_FILTER_20HZ = 2;
                LPMS_LOW_FILTER_4HZ = 3;
                LPMS_LOW_FILTER_2HZ = 4;
                LPMS_LOW_FILTER_04HZ = 5;
        */
        public int getLowPassFilter()
        {
            return lowPassFilter;
        }

        /*
           Function: setLowPassFilter( int lowpass)
           set sensor low pass filter settings
           Parameters: 
               int low pass filter settings

                low pass filter settings list:
                LPMS_LOW_FILTER_OFF = 0;
                LPMS_LOW_FILTER_40HZ = 1;
                LPMS_LOW_FILTER_20HZ = 2;
                LPMS_LOW_FILTER_4HZ = 3;
                LPMS_LOW_FILTER_2HZ = 4;
                LPMS_LOW_FILTER_04HZ = 5;
           Returns:
                bool:  true: success  false: fail
        */
        public bool setLowPassFilter( int lowpass)
        {
            if (!_assertConnected())
                return false;
            if (lowpass == LPMS_LOW_FILTER_OFF ||
                    lowpass == LPMS_LOW_FILTER_40HZ ||
                    lowpass == LPMS_LOW_FILTER_20HZ ||
                    lowpass == LPMS_LOW_FILTER_4HZ ||
                    lowpass == LPMS_LOW_FILTER_2HZ ||
                    lowpass == LPMS_LOW_FILTER_04HZ)
            {

                //ThreadPool.QueueUserWorkItem(new WaitCallback(SendingCommandThread), new CommandParameter(SET_RAW_DATA_LP, lowpass));

                actStatus = ACK_WAIT;
                lpbusSetInt32(SET_RAW_DATA_LP, lowpass);
                if (_waitForAckLoop() == ACK_RECEIVED)
                {
                    lowPassFilter = lowpass;
                    return true;
                }
            }
            return false;
        }

        /*
            Function: getStreamFrequency()
            get sensor streaming frequency
            Parameters:
                none
            Returns:
                int data stream frequency

                stream frequency list:
                LPMS_STREAM_FREQ_5HZ = 5;
                LPMS_STREAM_FREQ_10HZ = 10;
                LPMS_STREAM_FREQ_25HZ = 25;
                LPMS_STREAM_FREQ_50HZ = 50;
                LPMS_STREAM_FREQ_100HZ = 100;
                LPMS_STREAM_FREQ_200HZ = 200;
                LPMS_STREAM_FREQ_400HZ = 400;
        */
        public int getStreamFrequency()
        {
            return streamingFrequency;
        }

        /*
           Function: setStreamFrequency(int freq)
           set sensor data stream frequency
           Parameters: 
               int data stream frequency

                stream frequency list:
                LPMS_STREAM_FREQ_5HZ = 5;
                LPMS_STREAM_FREQ_10HZ = 10;
                LPMS_STREAM_FREQ_25HZ = 25;
                LPMS_STREAM_FREQ_50HZ = 50;
                LPMS_STREAM_FREQ_100HZ = 100;
                LPMS_STREAM_FREQ_200HZ = 200;
                LPMS_STREAM_FREQ_400HZ = 400;
           Returns:
                bool:  true: success  false: fail
        */
        public bool setStreamFrequency(int freq)
        {
            if (!_assertConnected())
                return false;
            if (freq == LPMS_STREAM_FREQ_5HZ ||
                    freq == LPMS_STREAM_FREQ_10HZ ||
                    freq == LPMS_STREAM_FREQ_25HZ ||
                    freq == LPMS_STREAM_FREQ_50HZ ||
                    freq == LPMS_STREAM_FREQ_100HZ ||
                    freq == LPMS_STREAM_FREQ_200HZ ||
                    freq == LPMS_STREAM_FREQ_400HZ)
            {
               // ThreadPool.QueueUserWorkItem(new WaitCallback(SendingCommandThread), new CommandParameter(SET_STREAM_FREQ, freq));

                actStatus = ACK_WAIT;
                lpbusSetInt32(SET_STREAM_FREQ, freq);
                if (_waitForAckLoop() == ACK_RECEIVED)
                {
                    _getConfig();
                    return true;
                }
            }

            return false;
        }

        /*
            Function: setTransmissionData( int v)
            set sensor transmission data
            Parameters:
                int transmissionData
                
                Transmission data list:             
                    LPMS_LINACC_OUTPUT_ENABLED
                    LPMS_DYNAMIC_COVAR_ENABLED
                    LPMS_ALTITUDE_OUTPUT_ENABLED
                    LPMS_QUAT_OUTPUT_ENABLED
                    LPMS_EULER_OUTPUT_ENABLED
                    LPMS_ANGULAR_VELOCITY_OUTPUT_ENABLED
                    LPMS_HEAVEMOTION_OUTPUT_ENABLED
                    LPMS_TEMPERATURE_OUTPUT_ENABLED
                    LPMS_GYR_RAW_OUTPUT_ENABLED
                    LPMS_ACC_RAW_OUTPUT_ENABLED
                    LPMS_MAG_RAW_OUTPUT_ENABLED
                    LPMS_PRESSURE_OUTPUT_ENABLED

                Example: to enable acc, gyr, quaternion and euler:

                    int transmissionData = 0;
                    transmissionData |= LPMS_ACC_RAW_OUTPUT_ENABLED;
                    transmissionData |= LPMS_GYR_RAW_OUTPUT_ENABLED;
                    transmissionData |= LPMS_QUAT_OUTPUT_ENABLED;
                    transmissionData |= LPMS_EULER_OUTPUT_ENABLED;
                    setTransmissionData(transmissionData);

            Returns:
                bool:  true: success  false: fail
        */
        public bool setTransmissionData( int v)
        {
            if (!_assertConnected())
                return false;

            actStatus = ACK_WAIT;
            lpbusSetInt32(SET_TRANSMIT_DATA, v);
            if (_waitForAckLoop() == ACK_RECEIVED)
            {
                _getConfig();
                return true;
            }

            return false;
        }

        /*
            Function: enableGyroData(bool b)
            set gyro data output. This command will leave other data output unchanged
            Parameters:
                bool b: true: enable | false: disable
            Returns:
                bool:  true: success  false: fail
        */
        public bool enableGyroData(bool b)
        {
            if (!_assertConnected())
                return false;
            return _enableConfigUtil(b, LPMS_GYR_RAW_OUTPUT_ENABLED);
        }

        /*
            Function: isGyroDataEnabled()
            check gyro data output settings
            Parameters:
                none
            Returns:
                bool b: true: enable | false: disable
       */
        public bool isGyroDataEnabled()
        {
            return gyrEnable;
        }

        /*
            Function: enableAccData(bool b)
            set cc data output. This command will leave other data output unchanged
            Parameters:
                bool b: true: enable | false: disable
            Returns:
                bool:  true: success  false: fail
        */
        public bool enableAccData(bool b)
        {
            if (!_assertConnected())
                return false;
            return _enableConfigUtil(b, LPMS_ACC_RAW_OUTPUT_ENABLED);
        }

        /*
            Function: isAccDataEnabled()
            check acc data output settings
            Parameters:
                none
            Returns:
                bool b: true: enable | false: disable
       */
        public bool isAccDataEnabled()
        {
            return accEnable;
        }

        /*
            Function: enableMagData(bool b)
            set mag data output. This command will leave other data output unchanged
            Parameters:
                bool b: true: enable | false: disable
            Returns:
                bool:  true: success  false: fail
        */
        public bool enableMagData(bool b)
        {
            if (!_assertConnected())
                return false;
            return _enableConfigUtil(b, LPMS_MAG_RAW_OUTPUT_ENABLED);
        }

        /*
            Function: isMagDataEnabled()
            check mag data output settings
            Parameters:
                none
            Returns:
                bool b: true: enable | false: disable
       */
        public bool isMagDataEnabled()
        {
            return magEnable;
        }

        /*
            Function: enableAngularVelData(bool b)
            set angular velocity data output. This command will leave other data output unchanged
            Parameters:
                bool b: true: enable | false: disable
            Returns:
                bool:  true: success  false: fail
        */
        public bool enableAngularVelData(bool b)
        {
            if (!_assertConnected())
                return false;
            return _enableConfigUtil(b, LPMS_ANGULAR_VELOCITY_OUTPUT_ENABLED);
        }

        /*
            Function: isAngularVelDataEnable()
            check angular velocity data output settings
            Parameters:
                none
            Returns:
                bool b: true: enable | false: disable
       */
        public bool isAngularVelDataEnable()
        {
            return angularVelEnable;
        }

        /*
            Function: enableQuaternionData(bool b)
            set quaternion data output. This command will leave other data output unchanged
            Parameters:
                bool b: true: enable | false: disable
            Returns:
                bool:  true: success  false: fail
        */
        public bool enableQuaternionData(bool b)
        {
            if (!_assertConnected())
                return false;
            return _enableConfigUtil(b, LPMS_QUAT_OUTPUT_ENABLED);
        }

        /*
            Function: isQuaternionDataEnabled()
            check quaternion data output settings
            Parameters:
                none
            Returns:
                bool b: true: enable | false: disable
        */
        public bool isQuaternionDataEnabled()
        {
            return quaternionEnable;
        }

        /*
            Function: enableEulerData(bool b)
            set euler data output. This command will leave other data output unchanged
            Parameters:
                bool b: true: enable | false: disable
            Returns:
                bool:  true: success  false: fail
        */
        public bool enableEulerData(bool b)
        {
            if (!_assertConnected())
                return false;
            return _enableConfigUtil(b, LPMS_EULER_OUTPUT_ENABLED);
        }

        /*
            Function: isEulerDataEnabled()
            check euler data output settings
            Parameters:
                none
            Returns:
                bool b: true: enable | false: disable
        */
        public bool isEulerDataEnabled()
        {
            return eulerAngleEnable;
        }

        /*
            Function: enableLinAccData(bool b)
            set linear acceleration data output. This command will leave other data output unchanged
            Parameters:
                bool b: true: enable | false: disable
            Returns:
                bool:  true: success  false: fail
        */
        public bool enableLinAccData(bool b)
        {
            if (!_assertConnected())
                return false;
            return _enableConfigUtil(b, LPMS_LINACC_OUTPUT_ENABLED);
        }

        /*
            Function: isLinAccDataEnabled()
            check linear acceleration data output settings
            Parameters:
                none
            Returns:
                bool b: true: enable | false: disable
        */
        public bool isLinAccDataEnabled()
        {
            return linAccEnable;
        }

        /*
            Function: enablePressureData(bool b)
            set pressure data output. This command will leave other data output unchanged
            Parameters:
                bool b: true: enable | false: disable
            Returns:
                bool:  true: success  false: fail
        */
        public bool enablePressureData(bool b)
        {
            if (!_assertConnected())
                return false;
            return _enableConfigUtil(b, LPMS_PRESSURE_OUTPUT_ENABLED);
        }

        /*
            Function: isPressureDataEnabled()
            check pressure data output settings
            Parameters:
                none
            Returns:
                bool b: true: enable | false: disable
        */
        public bool isPressureDataEnabled()
        {
            return pressureEnable;
        }

        /*
            Function: enableAltitudeData(bool b)
            set altitude data output. This command will leave other data output unchanged
            Parameters:
                bool b: true: enable | false: disable
            Returns:
                bool:  true: success  false: fail
        */
        public bool enableAltitudeData(bool b)
        {
            if (!_assertConnected())
                return false;
            return _enableConfigUtil(b, LPMS_ALTITUDE_OUTPUT_ENABLED);
        }

        /*
            Function: isAltitudeDataEnabled()
            check altitude data output settings
            Parameters:
                none
            Returns:
                bool b: true: enable | false: disable
        */
        public bool isAltitudeDataEnabled()
        {
            return altitudeEnable;
        }

        /*
            Function: enableTemperatureData(bool b)
            set temperature data output. This command will leave other data output unchanged
            Parameters:
                bool b: true: enable | false: disable
            Returns:
                bool:  true: success  false: fail
        */
        public bool enableTemperatureData(bool b)
        {
            if (!_assertConnected())
                return false;
            return  _enableConfigUtil(b, LPMS_TEMPERATURE_OUTPUT_ENABLED);
        }

        /*
            Function: isTemperatureDataEnabled()
            check temperature data output settings
            Parameters:
                none
            Returns:
                bool b: true: enable | false: disable
        */
        public bool isTemperatureDataEnabled()
        {
            return temperatureEnable;
        }

        /*
            Function: enable16BitData()
            enable 16bit data output
            Parameters:
                none
            Returns:
                bool:  true: success  false: fail
        */
        public bool enable16BitData()
        {
            if (!_assertConnected())
                return false;
            return _setDataMode(1);
        }

        /*
            Function: enable32BitData()
            enable 32bit data output
            Parameters:
                none
            Returns:
                bool:  true: success  false: fail
        */
        public bool enable32BitData()
        {
            if (!_assertConnected())
                return false;
            return _setDataMode(0);
        }

        /*
            Function: is16BitDataEnabled()
            check is 16 bit data output
            Parameters:
                none
            Returns:
                bool b: true: enable | false: disable
        */
        public bool is16BitDataEnabled()
        {
            if (sixteenBitDataEnable)
                return true;
            else
                return false;
        }

        /*
            Function: hasNewData()
            check if there are new sensor data in queue
            Parameters:
               none
            Returns:
               int number of data package in queue
       */
        public int hasNewData()
        {
            int n;
            lock(lockObject) {
                n = dataQueue.Count;
            }
            return n;
        }

        /*
            Function: getLpmsBData()
            get earliest sensor data in queue if sensor is in streaming mode. 
            if no sensor data is available in queue, a null object will be returned.
            Parameters:
               none
            Returns:
               LpmsData oject 
        */
        public LpmsData getLpmsBData()
        {
            LpmsData d = null;
            if (!_assertConnected())
                return d;

            lock(lockObject){
                if (dataQueue.Count > 0)
                {
                    d = dataQueue.Dequeue();
                }
            }
            return d;
        }

        /*
            Function: pollLpmsBData()
            Poll sensor for latest sensor data. This function should be used when the sensor is in command mode.
            This function sends a command to sensor to retrieve latest sensor data. Use getLpmsBData() function to return the latest sensor data.
            Sample code:
                sensor.pollLpmsBData();
                int nData = sensorCtl.hasNewData();
                if (nData > 0)
                {
                    LpmsData sd = new LpmsData();
                    sd = (LpmsData)sensorCtl.getLpmsBData();
                }

            Parameters:
               none
            Returns:
               none 
        */
        public void pollLpmsBData()
        {
            if (!_assertConnected())
                return ;

            waitForData = true;
            lpbusSetNone(GET_SENSOR_DATA);
            if (verbose) Console.WriteLine(TAG+"  Send GET_SENSOR_DATA");
            _waitForDataLoop();
        }

        /*
            Function: clearDataQueue() 
            clear internal data queue. 
            Parameters:
                none
            Returns:
                none
        */
        public void clearDataQueue() 
        {
            lock (lockObject) 
            {
                while (dataQueue.Count > 0)
                {
                    dataQueue.Dequeue();
                }
            }
        }
        /*
           Function: getSerialNumber()
           get sensor serial number
           Parameters:
               none
           Returns:
               String 32 characters serial number
        */
        public String getSerialNumber()
        {
            return serialNumber;
        }

        /*
          Function: getDeviceName()
          get sensor name (bluetooth name)
          Parameters:
              none
          Returns:
              String sensor name
        */
        public String getDeviceName()
        {
            return deviceName;
        }

        /*
          Function: isStreamingMode()
          check if sensor is in streaming mode
          Parameters:
              none
          Returns:
              bool 
                true: in streaming mode
                false: in command mode
        */
        public bool isStreamingMode()
        {
            return isStreamMode;
        }

        /*
            Function: getConnectionStatus()
            get current sensor connection status
            Parameters:
                none
            Returns:
                int connectionStatus
                SENSOR_STATUS_CONNECTED = 1;
                SENSOR_STATUS_CONNECTING = 2;
                SENSOR_STATUS_DISCONNECTED = 3;
                SENSOR_STATUS_ERROR = 4;
        */
        public int getConnectionStatus()
        {
            return connectionStatus;
        }

        /*
            Function: getFirmwareInfo()
            get firmware version info
            Parameters:
                none
            Returns:
                String firmware version info
        */
        public String getFirmwareInfo()
        {
            return firmwareInfo;
        }

        /*
            Function: startSyncSensor()
            start sensor sync procedure
            Parameters:
                none
            Returns:
                bool:  true: success  false: fail
        */
        public bool startSyncSensor()
        {
            if (!_assertConnected())
                return false;
            lpbusSetNone(START_SYNC);
            actStatus = ACK_WAIT;
            if (_waitForAckLoop() == ACK_RECEIVED)
            {
                return true;
            }

            return false;
        }

        /*
            Function: stopSyncSensor()
            stop sensor sync procedure
            Parameters:
                none
            Returns:
                bool:  true: success  false: fail
        */
        public bool stopSyncSensor()
        {
            if (!_assertConnected())
                return false;
            actStatus = ACK_WAIT;
            lpbusSetNone(STOP_SYNC);
            if (_waitForAckLoop() == ACK_RECEIVED)
            {
                return true;
            }

            return false;
        }

        private void testPing()
        {
            if (!_assertConnected())
                return;
            lpbusSetNone(GET_PING);
        }

        /*
            Function: resetFactorySettings()
            reset sensor to factory settings
            Parameters:
                none
            Returns:
                bool:  true: success  false: fail
        */
        public bool resetFactorySettings()
        {
            if (!_assertConnected())
                return false;
            _setCommandMode();
            actStatus = ACK_WAIT;
            lpbusSetNone(RESTORE_FACTORY_VALUE);
            int ret = _waitForAckLoop();
            if (ret == ACK_RECEIVED)
            {
                _getSensorSettings();
            }
            if (isStreamMode)
                _setStreamingMode();

            return ret == ACK_RECEIVED? true: false;
        }

        /*
            Function: setOrientationOffset(int offset)
            set sensor offset 
            Parameters: 
                int offset option

                Object Offset: LPMS_OFFSET_MODE_OBJECT = 0;
                Heading Offset: LPMS_OFFSET_MODE_HEADING = 1;
                Alignment Offset: LPMS_OFFSET_MODE_ALIGNMENT = 2;
            Returns:
               bool:  true: success  false: fail
        */
        public bool setOrientationOffset(int offset)
        {
            if (!_assertConnected())
                return false;
            if (offset == LPMS_OFFSET_MODE_ALIGNMENT ||
                    offset == LPMS_OFFSET_MODE_HEADING ||
                    offset == LPMS_OFFSET_MODE_OBJECT)
            {
                actStatus = ACK_WAIT;
                lpbusSetInt32(SET_ORIENTATION_OFFSET, offset);
                if (_waitForAckLoop() == ACK_RECEIVED)
                {
                    return true;
                }
            }
            return false;
        }

        /*
            Function: resetOrientationOffset()
            reset sensor offset 
            Parameters: 
                none
            Returns:
               bool:  true: success  false: fail
        */
        public bool resetOrientationOffset()
        {
            if (!_assertConnected())
                return false;
            actStatus = ACK_WAIT;
            lpbusSetNone(RESET_ORIENTATION_OFFSET);
            if (_waitForAckLoop() == ACK_RECEIVED)
            {
                return true;
            }
            return false;    
        }

        /*
            Function: resetTimestamp()
            reset sensor timestamp
            Parameters: 
                none
            Returns:
               bool:  true: success  false: fail
        */
        public bool resetTimestamp()
        {
            if (!_assertConnected())
                return false;
            actStatus = ACK_WAIT;
            lpbusSetInt32(SET_TIME_STAMP, 0);
            if (_waitForAckLoop() == ACK_RECEIVED)
            {
                return true;
            }
            return false;    
        }

        /*
            Function: setTimestamp()
            set sensor timestamp counter
            Parameters: 
                int ts
                ts is an integer counter value, calculated according to the following:
                   ts = T(seconds) x 400
                To set timestamp to 1 second, ts is 400;
            Returns:
               bool:  true: success  false: fail
        */
        public bool setTimestamp(int ts)
        {
            if (!_assertConnected())
                return false;
            actStatus = ACK_WAIT;
            lpbusSetInt32(SET_TIME_STAMP, ts);
            if (_waitForAckLoop() == ACK_RECEIVED)
            {
                return true;
            }
            return false;    
        }

        /*
            Function: sendBatteryPercentage()
            send retrieve battery percentage command. User has to send this command before retrieving latest battery reading
            Parameters: 
                none
            Returns:
                bool:  true: success  false: fail
        */
        public bool sendBatteryPercentage()
        {
            if (!_assertConnected())
                return false;
            actStatus = ACK_WAIT;
            lpbusSetNone(GET_BATTERY_LEVEL);
            if (_waitForAckLoop() == ACK_RECEIVED)
            {
                return true;
            }
            return false;    
        }

        /*
           Function: sendBatteryVoltage()
           send retrieve battery coltagee command. User has to send this command before retrieving latest battery reading
           Parameters: 
                none
           Returns:
               bool:  true: success  false: fail
        */
        public bool sendBatteryVoltage()
        {
            if (!_assertConnected())
                return false;
            actStatus = ACK_WAIT;
            lpbusSetNone(GET_BATTERY_VOLTAGE);
            if (_waitForAckLoop() == ACK_RECEIVED)
            {
                return true;
            }
            return false;    
        }

        /*
            Function: getBatteryLevel()
            get battery percentage. Required to send sendBatteryPercentage() before reading battery data
            Parameters: 
                none
            Returns:
                float: remaining battery percentage
        */
        public float getBatteryLevel()
        {
            return mLpmsData.batteryLevel;
        }

        /*
            Function: getBatteryVoltage()
            get battery voltage. Required to send sendBatteryVoltage() before reading battery data
            Parameters: 
                none
            Returns:
                float: battery voltage
        */
        public float getBatteryVoltage()
        {
            return mLpmsData.batteryVoltage;
        }

        /*
            Function: getgetChargingStatus()
            get charging status
            Parameters: 
                none
            Returns:
                bool:  true: success  false: fail
        */
        public bool getChargingStatus()
        {
            if (!_assertConnected())
                return false;
            actStatus = ACK_WAIT;
            lpbusSetNone(GET_CHARGING_STATUS);
            if (_waitForAckLoop() == ACK_RECEIVED)
            {
                return true;
            }
            return false;
        }

        public String getErrorMessage()
        {
            return sensorErrorMessage;
        }


        //Flash Data Log
        public Boolean startDebugLogging()
        {
            if (!_assertConnected())
                return false;
            _setCommandMode();
            actStatus = ACK_WAIT;
            lpbusSetNone(START_DEBUG_LOGGING);
            int ret = _waitForAckLoop();
            if (isStreamMode)
                _setStreamingMode();
            return ret == ACK_RECEIVED? true : false;
        }

        public Boolean stopDebugLogging()
        {
            if (!_assertConnected())
                return false;
            _setCommandMode();
            actStatus = ACK_WAIT;
            lpbusSetNone(STOP_DEBUG_LOGGING);
            int ret = _waitForAckLoop();
            if (isStreamMode)
                _setStreamingMode();
            return ret == ACK_RECEIVED? true : false;
        }

        public Boolean clearDebugLog()
        {
            if (!_assertConnected())
                return false;
            _setCommandMode();
            actStatus = ACK_WAIT;
            lpbusSetNone(CLEAR_DEBUG_LOG);
            int ret = _waitForAckLoop();
            if (isStreamMode)
                _setStreamingMode();
            return ret == ACK_RECEIVED? true : false;
        }

        //Thread block
        public Boolean fullFlashErase()
        {
            if (!_assertConnected())
                return false;
            _setCommandMode();
            actStatus = ACK_WAIT;
            lpbusSetNone(FULL_FLASH_ERASE);
            int ret = _waitForAckLoopWithoutTimeout();
            if (isStreamMode)
                _setStreamingMode();
            return ret == ACK_RECEIVED? true : false;
        }

        public Boolean getDebugLogStatus()
        {
            if (!_assertConnected())
                return false;
            _setCommandMode();
            waitForData = true;
            lpbusSetNone(GET_DEBUG_LOGGING_STATUS);
            _waitForDataLoop();
            if (isStreamMode)
                _setStreamingMode();
            return debugLoggingStatus == 0 ? true : false;
        }

        public int getFlashState()
        {
            if (!_assertConnected())
                return flashState;
            _setCommandMode();
            waitForData = true;
            lpbusSetNone(GET_FLASH_MEMORY_STATE);
            _waitForDataLoop();
            if (isStreamMode)
                _setStreamingMode();
            return flashState;
        }

        public int getDebugLogSize()
        {
            if (!_assertConnected())
                return 0;
            _setCommandMode();
            actStatus = ACK_WAIT;
            lpbusSetNone(GET_DEBUG_LOG_SIZE);
            _waitForAckLoop();
            if (isStreamMode)
                _setStreamingMode();
            return debugLogSize;
        }


        public void getDebugLog(int indexStart, int indexStop)
        {
            if (!_assertConnected())
                return;
            _setCommandMode();
            frameCounter = 0;
            debugLogSize = indexStop - indexStart + 1;
            debugLogSizeIndex = 0;
            sendGetDebugLog();
        }

        public Boolean storeDebugLog(String fileName)
        {
            dl.startDebugLogging(fileName);

            int size = getDebugLogSize();
            if (size == 0)
            {
                return false;
            }

            if (getDebugLogStatus())
            {
                getDebugLog(0, size - 1);
            }
            return true;
        }

        public int getUpdataProgress()
        {
            return updateProgress;
        }

        void sendGetDebugLog()
        {
            int indexStop;
            if (debugLogSize - debugLogSizeIndex > 2)
            {
                indexStop = debugLogSizeIndex + 2;
            }
            else
            {
                indexStop = debugLogSizeIndex - 1;
            }
            byte[] dataBF = new byte[8];
            convertIntToTxbytes(debugLogSizeIndex, 0, dataBF);
            convertIntToTxbytes(indexStop, 4, dataBF);

            lpbusSetData(GET_DEBUG_LOG, 8, dataBF);
        }

        public Boolean isDebugSaving()
        {
            return dl.isDebugLogging();
        }

        public Boolean stopDebugSaving()
        {
            return dl.stopDebugLogging();
        }

        /*
           Function: printConfig()
           pretty print current sensor configuration
           Parameters: 
                none
           Returns:
               none
        */
        public void printConfig()
        {
            Console.WriteLine(TAG+"  SN:  " + serialNumber);
            Console.WriteLine(TAG+"  FW:  " + firmwareInfo);
            Console.WriteLine(TAG+"  DN:  " + deviceName);
            Console.WriteLine(TAG+"  ImuId:                   " + imuId);
            Console.WriteLine(TAG+"  StreamFreq:              " + streamingFrequency);
            Console.WriteLine(TAG+"  Gyro:                    " + gyrRange);
            Console.WriteLine(TAG+"  Acc:                     " + accRange);
            Console.WriteLine(TAG+"  Mag:                     " + magRange);

            if (gyrEnable)
            {
                Console.WriteLine(TAG+"  GYRO ENABLED            o");
            }
            else
            {
                Console.WriteLine(TAG+"  GYRO DISABLED           x");
            }
            if (accEnable)
            {
                Console.WriteLine(TAG+"  ACC ENABLED             o");
            }
            else
            {
                Console.WriteLine(TAG+"  ACC DISABLED            x");
            }
            if (magEnable)
            {
                Console.WriteLine(TAG+"  MAG ENABLED             o");
            }
            else
            {
                Console.WriteLine(TAG+"  MAG DISABLED            x");
            }
            if (angularVelEnable)
            {
                Console.WriteLine(TAG+"  AngVel ENABLED          o");
            }
            else
            {
                Console.WriteLine(TAG+"  AngVel DISABLED         x");
            }
            if (quaternionEnable)
            {
                Console.WriteLine(TAG+"  QUAT ENABLED            o");
            }
            else
            {
                Console.WriteLine(TAG+"  QUAT DISABLED           x");
            }
            if (eulerAngleEnable)
            {
                Console.WriteLine(TAG+"  EULER ENABLED           o");
            }
            else
            {
                Console.WriteLine(TAG+"  EULER DISABLED          x");
            }
            if (linAccEnable)
            {
                Console.WriteLine(TAG+"  LINACC ENABLED          o");
            }
            else
            {
                Console.WriteLine(TAG+"  LINACC DISABLED         x");
            }
            if (pressureEnable)
            {
                Console.WriteLine(TAG+"  PRESSURE ENABLED        o");
            }
            else
            {
                Console.WriteLine(TAG+"  PRESSURE DISABLED       x");
            }
            if (altitudeEnable)
            {
                Console.WriteLine(TAG+"  ALTITUDE ENABLED        o");
            }
            else
            {
                Console.WriteLine(TAG+"  ALTITUDE DISABLED       x");
            }
            if (temperatureEnable)
            {
                Console.WriteLine(TAG+"  TEMPERATURE ENABLED     o");
            }
            else
            {
                Console.WriteLine(TAG+"  TEMPERATURE DISABLED    x");
            }

            if (heaveEnable)
            {
                Console.WriteLine(TAG+"  heave ENABLED           o");
            }                                                 
            else                                              
            {                                                 
                Console.WriteLine(TAG+"  heave DISABLED          x");
            }                                                 
                                                              
            if (sixteenBitDataEnable)                         
            {                                                 
                Console.WriteLine(TAG+"  16 bit ENABLED          o");
            }                                                 
            else                                              
            {                                                 
                Console.WriteLine(TAG+"  16 bit DISABLED         x");
            }

        }

        /*
          Function: getConfigPretty()
          get pretty print of current sensor configuration
          Parameters: 
               none
          Returns:
              String sensorConfig
       */
        public String getConfigPretty()
        {
            String ret = "";
            ret += "  SN:  " + serialNumber + "\n";
            ret +=  "  FW:  " + firmwareInfo + "\n";
            ret += "  DN:  " + deviceName + "\n";
            ret += "  ImuId:                   " + imuId + "\n";
            ret += "  StreamFreq:              " + streamingFrequency + "\n";
            ret += "  Gyro:                    " + gyrRange + "\n";
            ret += "  Acc:                     " + accRange + "\n";
            ret += "  Mag:                     " + magRange + "\n";

            if (gyrEnable)
            {
                ret += "  GYRO ENABLED            o" + "\n";
            }
            else
            {
                ret += "  GYRO DISABLED           x" + "\n";
            }
            if (accEnable)
            {
                ret += "  ACC ENABLED             o" + "\n";
            }
            else
            {
                ret += "  ACC DISABLED            x" + "\n";
            }
            if (magEnable)
            {
                ret += "  MAG ENABLED             o" + "\n";
            }
            else
            {
                ret += "  MAG DISABLED            x" + "\n";
            }
            if (angularVelEnable)
            {
                ret += "  AngVel ENABLED          o" + "\n";
            }
            else
            {
                ret += "  AngVel DISABLED         x" + "\n";
            }
            if (quaternionEnable)
            {
                ret += "  QUAT ENABLED            o" + "\n";
            }
            else
            {
                ret += "  QUAT DISABLED           x" + "\n";
            }
            if (eulerAngleEnable)
            {
                ret += "  EULER ENABLED           o" + "\n";
            }
            else
            {
                ret += "  EULER DISABLED          x" + "\n";
            }
            if (linAccEnable)
            {
                ret += "  LINACC ENABLED          o" + "\n";
            }
            else
            {
                ret += "  LINACC DISABLED         x" + "\n";
            }
            if (pressureEnable)
            {
                ret += "  PRESSURE ENABLED        o" + "\n";
            }
            else
            {
                ret += "  PRESSURE DISABLED       x" + "\n";
            }
            if (altitudeEnable)
            {
                ret += "  ALTITUDE ENABLED        o" + "\n";
            }
            else
            {
                ret += "  ALTITUDE DISABLED       x" + "\n";
            }
            if (temperatureEnable)
            {
                ret += "  TEMPERATURE ENABLED     o" + "\n";
            }
            else
            {
                ret += "  TEMPERATURE DISABLED    x" + "\n";
            }

            if (heaveEnable)
            {
                ret += "  heave ENABLED           o" + "\n";
            }
            else
            {
                ret += "  heave DISABLED          x" + "\n";
            }

            if (sixteenBitDataEnable)
            {
                ret += "  16 bit ENABLED          o" + "\n";
            }
            else
            {
                ret += "  16 bit DISABLED         x" + "\n";
            }
            return ret;
        }

        //////////////////////////////////
        ///  LPMS Internal method
        //////////////////////////////
        private bool _checkBluetooth() {
            radio = BluetoothRadio.PrimaryRadio;
            if (radio == null)//check whether the computer bluetooth adapter can work
            {
                //Console.WriteLine("Can't detect the computer bluetooth adapter.");
                return false;
            }
            return true;
        }

        private void _initialization()
        {
            _setCommandMode();
            _getSerialNumber();
            _getDeviceName();
            _getFirmwareInfo();
            _getSensorSettings();
            _setStreamingMode();
        }

        //without changing isStreamMode
        private void _setCommandMode()
        {
            if (!_assertConnected())
                return;
            actStatus = ACK_WAIT;
            if (verbose) Console.WriteLine(TAG+"   Send __GOTO_COMMAND_MODE");
            lpbusSetNone(GOTO_COMMAND_MODE);
            _waitForAckLoop();
        }

        private void _setStreamingMode()
        {
            if (!_assertConnected())
                return;
            actStatus = ACK_WAIT;
            if (verbose) Console.WriteLine(TAG+"  Send __GOTO_STREAM_MODE");
            lpbusSetNone(GOTO_STREAM_MODE);
            _waitForAckLoop();
        }

        private void _getGyroRange()
        {
            waitForData = true;
            if (verbose) Console.WriteLine(TAG+"  Send GET_GYR_RANGE");
            lpbusSetNone(GET_GYR_RANGE);
            _waitForDataLoop();
            if (verbose) Console.WriteLine(TAG+"  Gyro range: " + gyrRange + "dps");
        }

        private bool _enableConfigUtil(bool b, int contents)
        {
            if (!_assertConnected())
                return false;
            if (b)
                configurationRegister |= contents;
            else
                configurationRegister &= ~contents;
            return _setTransmissionData();
        }

        private void _getAccRange()
        {
            waitForData = true;
            if (verbose) Console.WriteLine(TAG+"  Send GET_ACC_RANGE");
            lpbusSetNone(GET_ACC_RANGE);
            _waitForDataLoop();
            if (verbose) Console.WriteLine(TAG+"  Acc range: " + accRange + "g");
        }

        private void _getMagRange()
        {
            waitForData = true;
            if (verbose) Console.WriteLine(TAG+"  Send GET_MAG_RANGE");
            lpbusSetNone(GET_MAG_RANGE);
            _waitForDataLoop();
            if (verbose) Console.WriteLine(TAG+"  Mag range: " + magRange + "mT");
        }

        private void _getFilterMode()
        {
            waitForData = true;
            if (verbose) Console.WriteLine(TAG+"  Send GET_FILTER_MODE");
            lpbusSetNone(GET_FILTER_MODE);
            _waitForDataLoop();
            if (verbose) Console.WriteLine(TAG+"  Filter mode: " + filterMode);
        }

        private void _getMagCorrection()
        {
            waitForData = true;
            if (verbose) Console.WriteLine(TAG+"  Send GET_FILTER_PRESET");
            lpbusSetNone(GET_FILTER_PRESET);
            _waitForDataLoop();
            if (verbose) Console.WriteLine(TAG+"  MagCorrection: " + magCorrection);
        }

        private void _getLowPassFilter()
        {
            waitForData = true;
            if (verbose) Console.WriteLine(TAG+"  Send GET_RAW_DATA_LP");
            lpbusSetNone(GET_RAW_DATA_LP);
            _waitForDataLoop();
            if (verbose) Console.WriteLine(TAG+"  Low-Pass Filter Mode: " + lowPassFilter);
        }

        private void _getSerialNumber()
        {
            serialNumberReady = false;
            if (verbose) Console.WriteLine(TAG+"  Send GET_SERIAL_NUMBER");
            lpbusSetNone(GET_SERIAL_NUMBER);
            int timeout = 0;
            while (timeout++ < 30 && !serialNumberReady)
            {
                try
                {
                    Thread.Sleep(PARAMETER_SET_DELAY);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }
        private void _getDeviceName()
        {
            deviceNameReady = false;
            if (verbose) Console.WriteLine(TAG+"  Send GET_DEVICE_NAME");
            lpbusSetNone(GET_DEVICE_NAME);
            int timeout = 0;
            while (timeout++ < 30 && !deviceNameReady)
            {
                try
                {
                    Thread.Sleep(PARAMETER_SET_DELAY);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        private void _getFirmwareInfo()
        {
            firmwareInfoReady = false;
            if (verbose) Console.WriteLine(TAG+"  Send GET_FIRMWARE_INFO");
            lpbusSetNone(GET_FIRMWARE_INFO);
            int timeout = 0;
            while (timeout++ < 30 && !firmwareInfoReady)
            {
                try
                {
                    Thread.Sleep(PARAMETER_SET_DELAY);
                }
                catch (Exception e)
                {
                    // TODO Auto-generated catch block
                    Console.WriteLine(e);
                    //throw new RuntimeException(e);
                }
            }
        }

        private bool _saveParameters()
        {
            if (!_assertConnected())
                return false;

            actStatus = ACK_WAIT;
            if (verbose) Console.WriteLine(TAG+"  Send WRITE_REGISTERS");
            lpbusSetNone(WRITE_REGISTERS);
            if (_waitForAckLoop() == ACK_RECEIVED)
            {
                isStreamMode = false;
                return true;
            }

            return false;

        }

        private bool _setTransmissionData()
        {
            if (!_assertConnected())
                return false;

            actStatus = ACK_WAIT;
            lpbusSetInt32(SET_TRANSMIT_DATA, configurationRegister);
            if (_waitForAckLoop() == ACK_RECEIVED)
            {
                _getConfig();
                return true;
            }
            return false;
        }

        private bool _setDataMode(int identifier)
        {
            if (!_assertConnected())
                return false;

            actStatus = ACK_WAIT;
            lpbusSetInt32(SET_LPBUS_DATA_MODE, identifier);
            if (_waitForAckLoop() == ACK_RECEIVED)
            {
                _getConfig();
                return true;
            }
            return false;
        }

        private void _getSensorSettings()
        {
            _getConfig();
            _getGyroRange();
            _getAccRange();
            _getMagRange();
            _getFilterMode();
            _getMagCorrection();
            _getLowPassFilter();
            //sendBatteryPercentage();
            //printConfig();
        }

        private void _getConfig()
        {
            configurationRegisterReady = false;
            if (verbose) Console.WriteLine(TAG+"  Send GET_CONFIG");
            lpbusSetNone(GET_CONFIG);
            int timeout = 0;
            while (timeout++ < 30 && !configurationRegisterReady)
            {
                try
                {
                    Thread.Sleep(PARAMETER_SET_DELAY);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
            // Parse configuration
            parseConfig(configurationRegister);
        }

        private void parseConfig(int config)
        {
            // Stream frequency
            if ((configurationRegister & LPMS_STREAM_FREQ_MASK) == LPMS_STREAM_FREQ_5HZ_ENABLED)
                streamingFrequency = LPMS_STREAM_FREQ_5HZ;
            else if ((configurationRegister & LPMS_STREAM_FREQ_MASK) == LPMS_STREAM_FREQ_10HZ_ENABLED)
                streamingFrequency = LPMS_STREAM_FREQ_10HZ;
            else if ((configurationRegister & LPMS_STREAM_FREQ_MASK) == LPMS_STREAM_FREQ_25HZ_ENABLED)
                streamingFrequency = LPMS_STREAM_FREQ_25HZ;
            else if ((configurationRegister & LPMS_STREAM_FREQ_MASK) == LPMS_STREAM_FREQ_50HZ_ENABLED)
                streamingFrequency = LPMS_STREAM_FREQ_50HZ;
            else if ((configurationRegister & LPMS_STREAM_FREQ_MASK) == LPMS_STREAM_FREQ_100HZ_ENABLED)
                streamingFrequency = LPMS_STREAM_FREQ_100HZ;
            else if ((configurationRegister & LPMS_STREAM_FREQ_MASK) == LPMS_STREAM_FREQ_200HZ_ENABLED)
                streamingFrequency = LPMS_STREAM_FREQ_200HZ;
            else if ((configurationRegister & LPMS_STREAM_FREQ_MASK) == LPMS_STREAM_FREQ_400HZ_ENABLED)
                streamingFrequency = LPMS_STREAM_FREQ_400HZ;

            if ((config & LPMS_GYR_RAW_OUTPUT_ENABLED) != 0)
            {
                gyrEnable = true;
            }
            else
            {
                gyrEnable = false;
            }
            if ((config & LPMS_ACC_RAW_OUTPUT_ENABLED) != 0)
            {
                accEnable = true;
            }
            else
            {
                accEnable = false;
            }
            if ((config & LPMS_MAG_RAW_OUTPUT_ENABLED) != 0)
            {
                magEnable = true;
            }
            else
            {
                magEnable = false;
            }
            if ((config & LPMS_ANGULAR_VELOCITY_OUTPUT_ENABLED) != 0)
            {
                angularVelEnable = true;
            }
            else
            {
                angularVelEnable = false;
            }
            if ((config & LPMS_QUAT_OUTPUT_ENABLED) != 0)
            {
                quaternionEnable = true;
            }
            else
            {
                quaternionEnable = false;
            }
            if ((config & LPMS_EULER_OUTPUT_ENABLED) != 0)
            {
                eulerAngleEnable = true;
            }
            else
            {
                eulerAngleEnable = false;
            }
            if ((config & LPMS_LINACC_OUTPUT_ENABLED) != 0)
            {
                linAccEnable = true;
            }
            else
            {
                linAccEnable = false;
            }
            if ((config & LPMS_PRESSURE_OUTPUT_ENABLED) != 0)
            {
                pressureEnable = true;
            }
            else
            {
                pressureEnable = false;
            }

            if ((config & LPMS_TEMPERATURE_OUTPUT_ENABLED) != 0)
            {
                temperatureEnable = true;
            }
            else
            {
                temperatureEnable = false;
            }

            if ((config & LPMS_ALTITUDE_OUTPUT_ENABLED) != 0)
            {
                altitudeEnable = true;
            }
            else
            {
                altitudeEnable = false;
            }
            if ((config & LPMS_HEAVEMOTION_OUTPUT_ENABLED) != 0)
            {
                heaveEnable = true;
            }
            else
            {
                heaveEnable = false;
            }
            if ((config & LPMS_LPBUS_DATA_MODE_16BIT_ENABLED) != 0)
            {
                sixteenBitDataEnable = true;
            }
            else
            {
                sixteenBitDataEnable = false;
            }
        }

        /////////////////////////////////////
        // LP Bus Related
        //////////////////////////////////////
        void lpbusSetInt32(int command, int v)
        {
            for (int i = 0; i < 4; ++i)
            {
                rawTxData[i] = (byte)(v & 0xff);
                v = v >> 8;
            }
            sendData(command, 4);
        }

        void lpbusSetNone(int command)
        {
            sendData(command, 0);
        }

        void lpbusSetData(int command, int length, byte[] dataBuffer)
        {
            for (int i = 0; i < length; ++i)
            {
                rawTxData[i] = dataBuffer[i];
            }
            sendData(command, length);
        }

        void sendData(int function, int length)
        {
            int txLrcCheck;

            txBuffer[0] = 0x3a;
            convertInt16ToTxbytes((Int16)imuId, 1, ref txBuffer);
            convertInt16ToTxbytes((Int16)function, 3, ref txBuffer);
            convertInt16ToTxbytes((Int16)length, 5, ref txBuffer);

            for (int i = 0; i < length; ++i)
            {
                txBuffer[7 + i] = rawTxData[i];
            }

            txLrcCheck = (imuId & 0xffff) + (function & 0xffff) + (length & 0xffff);

            for (int j = 0; j < length; j++)
            {
                txLrcCheck += (int)rawTxData[j] & 0xff;
            }

            convertInt16ToTxbytes((Int16)txLrcCheck, 7 + length, ref txBuffer);
            txBuffer[9 + length] = 0x0d;
            txBuffer[10 + length] = 0x0a;

            String s = BitConverter.ToString(txBuffer,0,length+11);    
            
            try
            {
                if (btCli.Connected)
                {
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Sending data ：" + s);
                    btStream.Write(txBuffer, 0, length + 11);
                }
                else
                {
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Sending data not work");
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
            }
        }

        void sendAck()
        {
            sendData(REPLY_ACK, 0);
        }

        void sendNack()
        {
            sendData(REPLY_NACK, 0);
        }

        void convertIntToTxbytes(int v, int offset, byte []buffer)
        {
            byte[] t_flip = BitConverter.GetBytes(v);

            byte[] t = new byte[4];
            t[0] = t_flip[3];
            t[1] = t_flip[2];
            t[2] = t_flip[1];
            t[3] = t_flip[0];

            for (int i = 0; i < 4; i++)
            {
                buffer[3 - i + offset] = t[i];
            }
        }

        private void parseFunction()
        {
            switch (currentFunction)
            {
                case REPLY_ACK:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received Ack");
                    actStatus = ACK_RECEIVED;
                    break;

                case REPLY_NACK:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received Nack");
                    actStatus = NACK_RECEIVED;
                    break;

                case GET_CONFIG:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received Config");
                    configurationRegister = convertRxbytesToInt(0, rxBuffer);
                    configurationRegisterReady = true;
                    break;

                case GET_STATUS:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received Status");
                    waitForData = false;
                    break;

                case GET_SENSOR_DATA:
                    if ((configurationRegister & LPMS_LPBUS_DATA_MODE_16BIT_ENABLED) != 0)
                    {
                        parseSensorData16Bit();
                    }
                    else
                    {
                        parseSensorData();
                    }
                    waitForData = false;
                    break;

                case GET_IMU_ID:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received ID");
                    imuId = convertRxbytesToInt(0, rxBuffer);
                    waitForData = false;
                    break;

                case GET_GYR_RANGE:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received Gyro range");
                    gyrRange = convertRxbytesToInt(0, rxBuffer);
                    waitForData = false;
                    break;

                case GET_ACC_RANGE:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received acc range");
                    accRange = convertRxbytesToInt(0, rxBuffer);
                    waitForData = false;
                    break;

                case GET_MAG_RANGE:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received mag range");
                    magRange = convertRxbytesToInt(0, rxBuffer);
                    waitForData = false;
                    break;

                case GET_FILTER_MODE:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received filter mode range");
                    filterMode = convertRxbytesToInt(0, rxBuffer);
                    waitForData = false;
                    break;

                case GET_FILTER_PRESET:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received filter preset");
                    magCorrection = convertRxbytesToInt(0, rxBuffer);
                    waitForData = false;
                    break;

                case GET_RAW_DATA_LP:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received raw data lp");
                    lowPassFilter = convertRxbytesToInt(0, rxBuffer);
                    waitForData = false;
                    break;
                    
                case GET_BATTERY_LEVEL:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received bat level");
                    mLpmsData.batteryLevel = convertRxbytesToFloat(0, rxBuffer);
                    waitForData = false;
                    break;

                case GET_CHARGING_STATUS:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received chargingStatus");
                    mLpmsData.chargingStatus = convertRxbytesToInt(0, rxBuffer);
                    waitForData = false;
                    break;

                case GET_BATTERY_VOLTAGE:
                    Console.WriteLine(TAG + "[LpmsB2Thread] Received bat vol");
                    mLpmsData.batteryVoltage = convertRxbytesToFloat(0, rxBuffer);
                    waitForData = false;
                    break;

                case GET_SERIAL_NUMBER:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received serial number");
                    serialNumber = convertRxbytesToString(currentLength, rxBuffer);
                    serialNumberReady = true;
                    waitForData = false;
                    break;

                case GET_DEVICE_NAME:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received device name");
                    deviceName = convertRxbytesToString(currentLength, rxBuffer);
                    deviceNameReady = true;
                    waitForData = false;
                    break;

                case GET_FIRMWARE_INFO:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received firmware info");
                    firmwareInfo = convertRxbytesToString(currentLength, rxBuffer);
                    firmwareInfoReady = true;
                    waitForData = false;
                    break;

                case GET_FIRMWARE_VERSION:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received firmware version");
                    int vmajor = convertRxbytesToInt(8, rxBuffer);
                    int vminor = convertRxbytesToInt(4, rxBuffer);
                    int vbuild = convertRxbytesToInt(0, rxBuffer);
                    firmwareVersion = vmajor.ToString() + "." + vminor.ToString() + "." + vbuild.ToString();
                    waitForData = false;
                    break;

                case GET_PING:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received ping");
                    float mT;
                    mT = (float)convertRxbytesToInt(0, rxBuffer) * 0.0025f;
                    waitForData = false;
                    break;

                case START_SYNC:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received start sync");
                    actStatus = ACK_RECEIVED;
                    break;

                case STOP_SYNC:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received stop sync");
                    actStatus = ACK_RECEIVED;
                    break;

                case SET_TRANSMIT_DATA:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received transmit data");
                    waitForData = false;
                    break;

                case GET_TEMPERATURE:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Received temperature");
                    mLpmsData.temperature = convertRxbytesToFloat(0, rxBuffer);
                    waitForData = false;
                    break;

                case GET_DEBUG_LOGGING_STATUS:
                    debugLoggingStatus = convertRxbytesToInt(0, rxBuffer);
                    waitForData = false;
                    break;

                case GET_DEBUG_LOG_SIZE:
                    debugLogSize = convertRxbytesToInt(0, rxBuffer) / LogEntrySize;
                    waitForData = false;
                    break;

                case GET_DEBUG_LOG:
                    int nData = currentLength / 64;
                    parseDebugSensorData(nData);
                    debugLogSizeIndex += nData;
                    updateProgress = debugLogSizeIndex * 100 / debugLogSize;
                    if (!dl.isDebugLogging() || debugLogSizeIndex >= debugLogSize)
                    {
                        dl.stopDebugLogging();
                        if (isStreamMode)
                            _setStreamingMode();
                    }
                    else
                    {
                        sendGetDebugLog();
                    }
                    waitForData = false;
                    break;

                case GET_FLASH_MEMORY_STATE:
                    flashState = convertRxbytesToInt(0, rxBuffer);
                    waitForData = false;
                    break;

                default:
                    if (verbose) Console.WriteLine(TAG + "[LpmsB2Thread] Unknown data");
                    break;
            }
        }

        void parse()
        {
            int lrcReceived = 0;
            for (int i = 0; i < nBytes; i++)
            {
                b = rawRxBuffer[i];
                
                switch (rxState)
                {
                    case PACKET_END:
                        if (b == 0x3a)
                        {
                            rxState = PACKET_ADDRESS0;
                        }
                        break;

                    case PACKET_ADDRESS0:
                        inBytes[0] = b;
                        rxState = PACKET_ADDRESS1;
                        break;

                    case PACKET_ADDRESS1:
                        inBytes[1] = b;
                        currentAddress = convertRxbytesToInt16(0, inBytes);
                        imuId = currentAddress;
                        rxState = PACKET_FUNCTION0;
                        break;

                    case PACKET_FUNCTION0:
                        inBytes[0] = b;
                        rxState = PACKET_FUNCTION1;
                        break;

                    case PACKET_FUNCTION1:
                        inBytes[1] = b;
                        currentFunction = convertRxbytesToInt16(0, inBytes);
                        rxState = PACKET_LENGTH0;
                        break;

                    case PACKET_LENGTH0:
                        inBytes[0] = b;
                        rxState = PACKET_LENGTH1;
                        break;

                    case PACKET_LENGTH1:
                        inBytes[1] = b;
                        currentLength = convertRxbytesToInt16(0, inBytes);
                        rxState = PACKET_RAW_DATA;
                        rxIndex = 0;
                        break;

                    case PACKET_RAW_DATA:
                        if (rxIndex == currentLength)
                        {
                            lrcCheck = (currentAddress & 0xffff) + (currentFunction & 0xffff) + (currentLength & 0xffff);
                            for (int j = 0; j < currentLength; j++)
                            {
                                if (j < MAX_BUFFER)
                                {
                                    lrcCheck += (int)rxBuffer[j] & 0xff;
                                }
                                else break;
                            }
                            inBytes[0] = b;
                            rxState = PACKET_LRC_CHECK1;
                        }
                        else
                        {
                            if (rxIndex < MAX_BUFFER)
                            {
                                rxBuffer[rxIndex] = b;
                                rxIndex++;
                            }
                            else break;
                        }
                        break;

                    case PACKET_LRC_CHECK1:
                        inBytes[1] = b;

                        lrcReceived = convertRxbytesToInt16(0, inBytes);
                        lrcCheck = lrcCheck & 0xffff;

                        if (lrcReceived == lrcCheck)
                        {
                            parseFunction();
                        }
                        rxState = PACKET_END;
                        break;

                    default:
                        rxState = PACKET_END;
                        break;
                }
            }
        }

        private void parseSensorData16Bit()
        {
            int o = 0;

            mLpmsData.imuId = imuId;
            mLpmsData.timestamp = (float)convertRxbytesToInt(0, rxBuffer) * 0.0025f;

            o += 4;
            mLpmsData.frameNumber = frameCounter;
            frameCounter++;

            if (gyrEnable)
            {
                for (int i = 0; i < 3; ++i)
                {
                    mLpmsData.gyr[i] = (float)((short)(((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 1000.0f * r2d;
                    o += 2;
                }
            }

            if (accEnable)
            {
                for (int i = 0; i < 3; ++i)
                {
                    mLpmsData.acc[i] = (float)((short)(((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 1000.0f;
                    o += 2;
                }
            }

            if (magEnable)
            {
                for (int i = 0; i < 3; ++i)
                {
                    mLpmsData.mag[i] = (float)((short)(((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 100.0f;
                    o += 2;
                }
            }

            if (angularVelEnable)
            {
                for (int i = 0; i < 3; ++i)
                {
                    mLpmsData.angVel[i] = (float)((short)(((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 1000.0f * r2d;
                    o += 2;
                }
            }

            if (quaternionEnable)
            {
                for (int i = 0; i < 4; ++i)
                {
                    mLpmsData.quat[i] = (float)((short)(((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 10000.0f;
                    o += 2;
                }
            }

            if (eulerAngleEnable)
            {
                for (int i = 0; i < 3; ++i)
                {
                    mLpmsData.euler[i] = (float)((short)(((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 10000.0f * r2d;
                    o += 2;
                }
            }

            if (linAccEnable)
            {
                for (int i = 0; i < 3; ++i)
                {
                    mLpmsData.linAcc[i] = (float)((short)(((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 1000.0f;
                    o += 2;
                }
            }

            if (pressureEnable)
            {
                mLpmsData.pressure = (float)((short)(((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 100.0f;
                o += 2;
            }

            if (altitudeEnable)
            {
                mLpmsData.altitude = (float)((short)(((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 10.0f;
                o += 2;
            }

            if (temperatureEnable)
            {
                mLpmsData.temperature = (float)((short)(((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 100.0f;
                o += 2;
            }

            if (heaveEnable)
            {
                mLpmsData.heave = (float)((short)(((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 1000.0f;
                o += 2;
            }

            lock(lockObject) {
                if (dataQueue.Count < DATA_QUEUE_SIZE)
                    dataQueue.Enqueue(new LpmsData(mLpmsData));
                else
                {
                    dataQueue.Dequeue();
                    dataQueue.Enqueue(new LpmsData(mLpmsData));
                }
            }

            newDataFlag = true;
        }

        void parseDebugSensorData(int nData)
        {
            int o = 0;
            for (int n = 0; n < nData; n++)
            {
                mLpmsData.frameNumber = frameCounter;
                frameCounter++;
                mLpmsData.timestamp = (float)convertRxbytesToInt(o, rxBuffer) * 0.0025f;
                o += 4;
                mLpmsData.acc[0] = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
                mLpmsData.acc[1] = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
                mLpmsData.acc[2] = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
                mLpmsData.gyr[0] = convertRxbytesToFloat(o, rxBuffer) * r2d;
                o += 4;
                mLpmsData.gyr[1] = convertRxbytesToFloat(o, rxBuffer) * r2d;
                o += 4;
                mLpmsData.gyr[2] = convertRxbytesToFloat(o, rxBuffer) * r2d;
                o += 4;
                mLpmsData.mag[0] = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
                mLpmsData.mag[1] = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
                mLpmsData.mag[2] = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
                mLpmsData.quat[0] = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
                mLpmsData.quat[1] = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
                mLpmsData.quat[2] = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
                mLpmsData.quat[3] = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
                mLpmsData.pressure = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
                mLpmsData.temperature = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
                dl.logDebugLpmsData(mLpmsData);
            }
        }

        private void parseSensorData()
        {
            int o = 0;

            mLpmsData.imuId = imuId;
            mLpmsData.timestamp = (float)convertRxbytesToInt(o, rxBuffer) * 0.0025f;
            o += 4;
            if (gyrEnable)
            {
                mLpmsData.gyr[0] = convertRxbytesToFloat(o, rxBuffer) * r2d;
                o += 4;
                mLpmsData.gyr[1] = convertRxbytesToFloat(o, rxBuffer) * r2d;
                o += 4;
                mLpmsData.gyr[2] = convertRxbytesToFloat(o, rxBuffer) * r2d;
                o += 4;
            }

            if (accEnable)
            {
                mLpmsData.acc[0] = (convertRxbytesToFloat(o, rxBuffer));
                o += 4;
                mLpmsData.acc[1] = (convertRxbytesToFloat(o, rxBuffer));
                o += 4;
                mLpmsData.acc[2] = (convertRxbytesToFloat(o, rxBuffer));
                o += 4;
            }

            if (magEnable)
            {
                mLpmsData.mag[0] = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
                mLpmsData.mag[1] = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
                mLpmsData.mag[2] = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
            }

            if (angularVelEnable)
            {
                mLpmsData.angVel[0] = convertRxbytesToFloat(o, rxBuffer) * r2d;
                o += 4;
                mLpmsData.angVel[1] = convertRxbytesToFloat(o, rxBuffer) * r2d;
                o += 4;
                mLpmsData.angVel[2] = convertRxbytesToFloat(o, rxBuffer) * r2d;
                o += 4;
            }

            if (quaternionEnable)
            {
                mLpmsData.quat[0] = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
                mLpmsData.quat[1] = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
                mLpmsData.quat[2] = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
                mLpmsData.quat[3] = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
            }

            if (eulerAngleEnable)
            {
                mLpmsData.euler[0] = convertRxbytesToFloat(o, rxBuffer) * r2d;
                o += 4;
                mLpmsData.euler[1] = convertRxbytesToFloat(o, rxBuffer) * r2d;
                o += 4;
                mLpmsData.euler[2] = convertRxbytesToFloat(o, rxBuffer) * r2d;
                o += 4;
            }

            if (linAccEnable)
            {
                mLpmsData.linAcc[0] = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
                mLpmsData.linAcc[1] = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
                mLpmsData.linAcc[2] = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
            }

            if (pressureEnable)
            {
                mLpmsData.pressure = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
            }

            if (altitudeEnable)
            {
                mLpmsData.altitude = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
            }

            if (temperatureEnable)
            {
                mLpmsData.temperature = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
            }

            if (heaveEnable)
            {
                mLpmsData.heave = convertRxbytesToFloat(o, rxBuffer);
                o += 4;
            }

            lock (lockObject)
            {
                if (dataQueue.Count < DATA_QUEUE_SIZE)
                    dataQueue.Enqueue(new LpmsData(mLpmsData));
                else
                {
                    dataQueue.Dequeue();
                    dataQueue.Enqueue(new LpmsData(mLpmsData));
                }
            }

            newDataFlag = true;
        }

        //////////////////////////////////////////
        /// BitConverter function
        //////////////////////////////////////////

        private float convertRxbytesToFloat(int offset, byte[] buffer)
        {
            return BitConverter.ToSingle(buffer,offset);
        }

        private int convertRxbytesToInt(int offset, byte[] buffer)
        {
            return BitConverter.ToInt32(buffer, offset);
        }

        private int convertRxbytesToInt16(int offset, byte[] buffer)
        {
            return BitConverter.ToInt16(buffer, offset);
        }

        private String convertRxbytesToString(int length, byte[] buffer)
        {
            //Max length define 32
            //return BitConverter.ToString(buffer,0,32);
            byte[] buffer2 = new byte[length];
            Buffer.BlockCopy(buffer, 0, buffer2, 0, length);
            return System.Text.Encoding.UTF8.GetString(buffer2);
        }

        private void convertInt16ToTxbytes(Int16 v,int offset,ref byte[] buffer) {
            byte[] b = BitConverter.GetBytes(v);
            for (int i = 0; i < 2; i++)
            {
                buffer[i+offset] = b[i];
            }
        }

        private int _waitForAckLoop()
        {
            int timeout = 0;
            while (timeout++ < 30 && 
                    actStatus == ACK_WAIT)
            {
                try
                {
                    Thread.Sleep(PARAMETER_SET_DELAY);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception :" + e);
                }
            }
            int ret = actStatus;
            actStatus = ACK_WAIT;
            return ret;
        }

        private void _waitForDataLoop()
        {
            int timeout = 0;
            while (timeout++ < 30 && waitForData)
            {
                try
                {
                    Thread.Sleep(PARAMETER_SET_DELAY);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception :" + e);
                }
            }
        }

        private int _waitForAckLoopWithoutTimeout()
        {
            while (actStatus == ACK_WAIT)
            {
                try
                {
                    Thread.Sleep(PARAMETER_SET_DELAY);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception :" + e);
                }
            }
            int ret = actStatus;
            actStatus = ACK_WAIT;
            return ret;
        }

        private bool _assertConnected()
        {
            if (connectionStatus == SENSOR_STATUS_CONNECTED)
                return true;
            return false;
        }

        private void _startSensorConnection()
        {
            // Connect to sensor
            BluetoothAddress btAddr = BluetoothAddress.Parse(mAddress);
            Guid serviceClass = BluetoothService.SerialPort;
            BluetoothEndPoint ep = new BluetoothEndPoint(btAddr, serviceClass);
            btCli = new BluetoothClient();
            try
            {
                btCli.Connect(ep);
            }
            catch (Exception e)
            {
                _setErrorMessage(e.ToString());
                connectionStatus = SENSOR_STATUS_ERROR;
                return;
            }

            try
            {
                Thread.Sleep(100); // wait for 0.8 second
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            btStream = btCli.GetStream();
            if (btStream == null)
            {
                _setErrorMessage("Error getting Bluetooth R/W Stream");
                connectionStatus = SENSOR_STATUS_ERROR;
                return;
            }

            dataReadThread = new Thread(_dataReceivedLoop);
            dataReadThread.Start();

            connectionStatus = SENSOR_STATUS_CONNECTED;
            _initialization();
            frameCounter = 0;
        }

        private void _dataReceivedLoop()
        {
            // Start infinite read loop
            try
            {
                while (btStream != null && btStream.CanRead && btCli.Connected)
                {
                    nBytes = btStream.Read(rawRxBuffer, 0, rawRxBuffer.Length);
                    if (nBytes > 0)
                        parse();
                }
            } 
            catch (IOException e)
            {
                connectionStatus = SENSOR_STATUS_ERROR;
            }
        }

        private void _setErrorMessage(String s)
        {
            sensorErrorMessage = s;
        }

        private void _resetErrorMessage()
        {
            String sensorErrorMessage = "none";
        }

    }
}