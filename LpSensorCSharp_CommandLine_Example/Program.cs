﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using LPMSB2;
using System.IO;

namespace LpSensorCSharp_CommandLine_Example
{
    class Program
    {

        static void Main(string[] args)
        {
            string sensor1Address = "00:04:3E:94:3D:91";

            // Initialize sensor manager
            LpmsB2 sensor1 = new LpmsB2(sensor1Address);

            // connects to sensor
            int retryCount = 0;
            sensor1.connect();
            while (sensor1.getConnectionStatus() != LpmsB2.SENSOR_STATUS_CONNECTED)
            {
                if (sensor1.getConnectionStatus() == LpmsB2.SENSOR_STATUS_ERROR)
                {
                    if (retryCount >= 3)
                    {
                        Console.WriteLine("Error connecting to sensor after {0} retries. Terminating bye", retryCount);
                        System.Environment.Exit(1);
                    }
                    Console.WriteLine("Error connecting to sensor: {0}", sensor1.getErrorMessage());
                    Console.WriteLine("retrying {0}", retryCount);
                    retryCount += 1;
                }
                Thread.Sleep(3000);
            }

            sensor1.printConfig();
            String filename = "LpmsB2Log_"+DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
            using (StreamWriter writetext = new StreamWriter(filename))
            {
                // Retrieve and print out sensor data which sensors are connected
                while (sensor1.getConnectionStatus() == LpmsB2.SENSOR_STATUS_CONNECTED)
                {

                    int nData = sensor1.hasNewData();
                    if (nData > 0)
                    {
                        LpmsData sd = new LpmsData();
                        sd = ((LpmsData)sensor1.getLpmsBData());

                        //file.WriteLine(s);
                        String s = String.Format("{0:0.000},{1:+0.000;-0.000},{2:+0.000;-0.000},{3:+0.000;-0.000},{4:+0.000;-0.000},{5:+0.000;-0.000},{6:+0.000;-0.000},{7:+0.000;-0.000},{8:+0.000;-0.000},{9:+0.000;-0.000},{10:+0.000;-0.000},{11}",
                            sd.timestamp, sd.quat[0], sd.quat[1], sd.quat[2], sd.quat[3],
                            sd.acc[0], sd.acc[1], sd.acc[2],
                            sd.gyr[0], sd.gyr[1], sd.gyr[2], nData);
                        Console.WriteLine(s);
                        writetext.WriteLine(s);
                    } else
                        System.Threading.Thread.Sleep(1);
                }
            }

            // Disconnects sensor1
            if (sensor1.disconnect())
                Console.WriteLine("sensor {0} disconnected", sensor1.getAddress());
            else
                Console.WriteLine("sensor {0} error disconnecting", sensor1.getAddress());
        }
    }
}
