﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using LPMSB2;
using System.IO;

namespace LpSensorCSharp_CommandLine_Example
{
    class Program
    {
        static void Main(string[] args)
        {
            //string sensor1Address = "00:04:3E:94:3D:91";
            string sensor1Address = "00:04:3E:9B:A3:6D";

            // Initialize sensor manager
            LpmsB2 sensor1 = new LpmsB2(sensor1Address);

            // connects to sensor
            int retryCount = 0;
            sensor1.connect();
            while (sensor1.getConnectionStatus() != LpmsB2.SENSOR_STATUS_CONNECTED)
            {
                if (sensor1.getConnectionStatus() == LpmsB2.SENSOR_STATUS_ERROR)
                {
                    if (retryCount >= 3)
                    {
                        Console.WriteLine("Error connecting to sensor after {0} retries. Terminating bye", retryCount);
                        System.Environment.Exit(1);
                    }
                    Console.WriteLine("Error connecting to sensor: {0}", sensor1.getErrorMessage());
                    Console.WriteLine("retrying {0}", retryCount);
                    retryCount += 1;
                }
                Thread.Sleep(3000);
            }

            sensor1.printConfig();
            String filename = "LpmsB2Log_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";

            Console.WriteLine("Deleting previous recordings");
            sensor1.clearDebugLog();

            // start flash logging
            sensor1.setStreamingMode();
            sensor1.startDebugLogging();

            Console.WriteLine("Started flash logging of Sensor, recording for 5 seconds");
            Thread.Sleep(5000);

            sensor1.stopDebugLogging();
            // set to command mode to have more bandwith for the download
            sensor1.setCommandMode();
            Console.WriteLine("Stopped loggging, recorded " + sensor1.getDebugLogSize().ToString() + " entries");

            if (sensor1.storeDebugLog(filename))
            {
                while (sensor1.isDebugSaving())
                {
                    Console.WriteLine("Downloading sensor data ... " + sensor1.getUpdataProgress().ToString() + "% done");
                    Thread.Sleep(500);
                }
            }

            Console.WriteLine("Clearing sensor logging flash");
            sensor1.clearDebugLog();

            // Disconnects sensor1
            if (sensor1.disconnect())
                Console.WriteLine("sensor {0} disconnected", sensor1.getAddress());
            else
                Console.WriteLine("sensor {0} error disconnecting", sensor1.getAddress());

            Thread.Sleep(2000);
        }
    }
}
