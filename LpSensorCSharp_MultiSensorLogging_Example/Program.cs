﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using LPMSB2;
using System.IO;
using System.Diagnostics;

namespace LpSensorCSharp_MultiSensorLogging_Example
{
    class Program
    {

        static void Main(string[] args)
        {
            string sensor1Address = "00:04:3E:30:37:0D";
            string sensor2Address = "00:04:3E:9B:A2:C2";

            //////////////////////////////////////////////////////////////////
            // Initialize sensor manager
            //////////////////////////////////////////////////////////////////
            LpmsB2 sensor1 = new LpmsB2(sensor1Address);
            LpmsB2 sensor2 = new LpmsB2(sensor2Address);

            //////////////////////////////////////////////////////////////////
            // connects to sensor
            //////////////////////////////////////////////////////////////////
            int retryCount = 0;

            // Connect to sensor1
            Console.WriteLine("Connecting to sensor1: " + sensor1Address);
            sensor1.connect();
            while (sensor1.getConnectionStatus() != LpmsB2.SENSOR_STATUS_CONNECTED)
            {
                if (sensor1.getConnectionStatus() == LpmsB2.SENSOR_STATUS_ERROR)
                {
                    if (retryCount >= 3)
                    {
                        Console.WriteLine("Error connecting to sensor after {0} retries. Terminating bye", retryCount);
                        System.Environment.Exit(1);
                    }
                    Console.WriteLine("Error connecting to sensor: {0}", sensor1.getErrorMessage());
                    Console.WriteLine("retrying {0}", retryCount);
                    retryCount += 1;
                }
                Thread.Sleep(3000);
            }
            // put sensor1 into command mode
            Console.WriteLine("Connected to sensor1: " + sensor2Address);
            Console.WriteLine("Setting sensor1 to command mode");
            sensor1.setCommandMode();


            // Connect to sensor2
            retryCount = 0;
            Console.WriteLine("\nConnecting to sensor2: " + sensor2Address);
            sensor2.connect();
            while (sensor2.getConnectionStatus() != LpmsB2.SENSOR_STATUS_CONNECTED)
            {
                if (sensor2.getConnectionStatus() == LpmsB2.SENSOR_STATUS_ERROR)
                {
                    if (retryCount >= 3)
                    {
                        Console.WriteLine("Error connecting to sensor after {0} retries. Terminating bye", retryCount);
                        System.Environment.Exit(1);
                    }
                    Console.WriteLine("Error connecting to sensor: {0}", sensor2.getErrorMessage());
                    Console.WriteLine("retrying {0}", retryCount);
                    retryCount += 1;
                }
                Thread.Sleep(3000);
            }
            // put sensor2 into command mode
            Console.WriteLine("Connected to sensor2: " + sensor1Address);
            Console.WriteLine("Setting sensor2 to command mode");
            sensor2.setCommandMode();

            //////////////////////////////////////////////////////////////////
            // clean up sensor data queue
            //////////////////////////////////////////////////////////////////
            Console.WriteLine("Clean up sensor queue");
            int n = sensor1.hasNewData();
            for (int i=0; i< n; ++i)
            {
                sensor1.getLpmsBData();
            }

            n = sensor2.hasNewData();
            for (int i = 0; i < n; ++i)
            {
                sensor2.getLpmsBData();
            }

            //////////////////////////////////////////////////////////////////
            // setup sensors
            //////////////////////////////////////////////////////////////////
            // stream freq
            Console.WriteLine("Setup streaming frequency");
            sensor1.setStreamFrequency(LpmsB2.LPMS_STREAM_FREQ_100HZ);
            sensor2.setStreamFrequency(LpmsB2.LPMS_STREAM_FREQ_100HZ);

            // Transmission data
            Console.WriteLine("Setup transmission data");
            int transmissionData = 0;
            transmissionData |= LpmsB2.LPMS_ACC_RAW_OUTPUT_ENABLED;
            transmissionData |= LpmsB2.LPMS_GYR_RAW_OUTPUT_ENABLED;
            transmissionData |= LpmsB2.LPMS_MAG_RAW_OUTPUT_ENABLED;
            transmissionData |= LpmsB2.LPMS_QUAT_OUTPUT_ENABLED;
            transmissionData |= LpmsB2.LPMS_EULER_OUTPUT_ENABLED;
            transmissionData |= LpmsB2.LPMS_LINACC_OUTPUT_ENABLED;
            sensor1.setTransmissionData(transmissionData);
            sensor2.setTransmissionData(transmissionData);


            sensor1.printConfig();
            sensor2.printConfig();

            Console.Write("\nPress any key to continue... ");
            Console.ReadLine();

            //////////////////////////////////////////////////////////////////
            // Sync sensors
            //////////////////////////////////////////////////////////////////
            Console.Write("Syncing sensors");
            sensor1.startSyncSensor();
            sensor2.startSyncSensor();
            for (int i=0; i<3; ++i)
            {
                System.Threading.Thread.Sleep(1000);
                Console.Write(".");
            }
            sensor1.stopSyncSensor();
            sensor2.stopSyncSensor();

            Console.WriteLine("Done");

            sensor1.setStreamingMode();
            sensor2.setStreamingMode();

            // Data analysis: Start stopwatch
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            // Data Logging
            String filename = "LpmsB2Log_"+DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";


            //////////////////////////////////////////////////////////////////
            // Main loop
            //////////////////////////////////////////////////////////////////
            using (StreamWriter writetext = new StreamWriter(filename))
            {
                LpmsData sd1 = new LpmsData();
                LpmsData sd2 = new LpmsData();

                // Data Logging
                writetext.WriteLine("Sensor,timestamp(s),accX(g),accY(g),accZ(g),gyrX(dps),gyrY(dps),gyrZ(dps),magX(uT),magY(uT),magZ(uT),qW,qX,qY,qZ,linAccX(g),linAccY(g),linAccZ(g)");
                string csvFormat = "{0:0.000}," +
                                "{1:+0.000;-0.000},{2:+0.000;-0.000},{3:+0.000;-0.000}," +
                                "{4:+0.000;-0.000},{5:+0.000;-0.000},{6:+0.000;-0.000}," +
                                "{7:+0.000;-0.000},{8:+0.000;-0.000},{9:+0.000;-0.000}," +
                                "{10:+0.000;-0.000},{11:+0.000;-0.000},{12:+0.000;-0.000},{13:+0.000; -0.000}," +
                                "{14:+0.000;-0.000},{15:+0.000;-0.000},{16:+0.000;-0.000}";

                // Data analysis
                int sensor1DataCount = 0;
                int sensor2DataCount = 0;
                Console.WriteLine("Sensor1_T(s) | Sensor2_T(s) | WallClock(s) | Sensor1_dt(s) | Sensor2_dt(s) | Sensor1_DataCount | Sensor2_DataCount | DataCount_Diff");

                // Retrieve and print out sensor data which sensors are connected
                while (sensor1.getConnectionStatus() == LpmsB2.SENSOR_STATUS_CONNECTED &&
                       sensor2.getConnectionStatus() == LpmsB2.SENSOR_STATUS_CONNECTED)
                {
                    // Check sensor1 data queue
                    int nDataSensor1 = sensor1.hasNewData();
                    if (nDataSensor1 > 0)
                    {
                        sd1 = ((LpmsData)sensor1.getLpmsBData());

                        String s = String.Format("1,"+csvFormat,
                            sd1.timestamp, 
                            sd1.acc[0], sd1.acc[1], sd1.acc[2],
                            sd1.gyr[0], sd1.gyr[1], sd1.gyr[2],
                            sd1.mag[0], sd1.mag[1], sd1.mag[2],
                            sd1.quat[0], sd1.quat[1], sd1.quat[2], sd1.quat[3], 
                            sd1.linAcc[0], sd1.linAcc[1], sd1.linAcc[2]);
                        sensor1DataCount++;

                        writetext.WriteLine(s);
                    } else
                    {
                        System.Threading.Thread.Sleep(1);
                    }

                    // Check sensor2 data queue
                    int nDataSensor2 = sensor2.hasNewData();
                    if (nDataSensor2 > 0)
                    {
                        sd2 = ((LpmsData)sensor2.getLpmsBData());

                        String s = String.Format("2," + csvFormat,
                            sd2.timestamp,
                            sd2.acc[0], sd2.acc[1], sd2.acc[2],
                            sd2.gyr[0], sd2.gyr[1], sd2.gyr[2],
                            sd2.mag[0], sd2.mag[1], sd2.mag[2],
                            sd2.quat[0], sd2.quat[1], sd2.quat[2], sd2.quat[3],
                            sd2.linAcc[0], sd2.linAcc[1], sd2.linAcc[2]);

                        sensor2DataCount++;
                        writetext.WriteLine(s);
                    }
                    else
                    {
                        System.Threading.Thread.Sleep(1);
                    }


                    // Data analysis
                    TimeSpan ts = stopWatch.Elapsed;
                    Console.Write("\r{0,12:###.00} | {1,12:###.00} | {2,12:###.00} | {3,13:###.00} | {4,13:###.00} | {5,17} | {6,17} | {7,14}   ",
                        sd1.timestamp, sd2.timestamp, ts.TotalSeconds, ts.TotalSeconds-sd1.timestamp, ts.TotalSeconds-sd2.timestamp, 
                        sensor1DataCount, sensor2DataCount, sensor1DataCount - sensor2DataCount);

                }
            }

            // Disconnects sensor1
            if (sensor1.disconnect())
                Console.WriteLine("sensor {0} disconnected", sensor1.getAddress());
            else
                Console.WriteLine("sensor {0} error disconnecting", sensor1.getAddress());

            // Disconnects sensor2
            if (sensor2.disconnect())
                Console.WriteLine("sensor {0} disconnected", sensor2.getAddress());
            else
                Console.WriteLine("sensor {0} error disconnecting", sensor2.getAddress());
        }
    }
}
